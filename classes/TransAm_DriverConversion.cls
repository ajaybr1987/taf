global class TransAm_DriverConversion {
    webService static Id cloneApplication(Id appId) {
       TransAm_Application__c oTransAmApp = new TransAm_Application__c();
        
        String ObjectFields = getFields('TransAm_Application__c');
        
        String sQuery = 'Select '+ObjectFields+' from TransAm_Application__c where id=: appId';
        System.debug('sQuery>>>>>>>>>>>>>>..'+sQuery );
        oTransAmApp = Database.Query(sQuery);
        
        TransAm_Application__c  cloneApp =  new TransAm_Application__c();
        cloneApp  = oTransAmApp.clone();
        if (cloneApp!=null){
        cloneApp.TransAm_EBE_Entry_ID__c = '';
        cloneApp.TransAm_DriverID__c = '';
        cloneApp.TransAm_Emp_Status__c = 'New';
        cloneApp.TransAm_Status_Reason__c = '';
        cloneApp.TransAm_Termination_Date__c = Null;
        cloneApp.TransAm_Status_Flag__c = false; 
        cloneApp.TransAm_Status__c = 'Application Received';
        cloneApp.TransAm_RVI_Export__c = FALSE;
	cloneApp.TransAm_RVI_Export_Date__c = NULL;
	cloneApp.TransAm_By_Pass_Validation__c = false;       
        try{
            insert cloneApp;
        }catch(exception e){
            TransAm_Error_Log__c el = new TransAm_Error_Log__c();
            el.TransAm_Class_Name__c = 'TransAm_DriverConversion';
            el.TransAm_Method_Name__c = 'cloneApplication';
            el.TransAm_Module_Name__c = 'Application';
            el.TransAm_Exception_Message__c = e.getMessage();
            insert el;
            }
        }
        
        list<Sobject>  listToUpdate = new list<Sobject>();
        list<TransAm_Application__c> appRelatedList= getRelatedList('TransAm_Application__c',appId,'TransAm_Master_Application__c');
        TransAm_Application__c oTranApp = new TransAm_Application__c();
        for(TransAm_Application__c oTransAm : appRelatedList){
        oTranApp = oTransAm.clone();
        oTranApp.TransAm_Master_Application__c = cloneApp.id; 
        oTranApp.TransAm_EBE_Entry_ID__c = '';
	oTranApp.TransAm_By_Pass_Validation__c = false;
        listToUpdate.Add(oTranApp);
        }
        
        list<TransAm_PreviousAddress__c> addRelatedList= getRelatedList('TransAm_PreviousAddress__c',appId,'TransAm_Application__c');
        TransAm_PreviousAddress__c TranAmAdd = new TransAm_PreviousAddress__c();
        for(TransAm_PreviousAddress__c oTransAmAdd : addRelatedList){
        TranAmAdd = oTransAmAdd.clone();
        TranAmAdd.TransAm_Application__c = cloneApp.id; 
        TranAmAdd.TransAm_EBE_Entry_Id__c = '';
        listToUpdate.Add(TranAmAdd );
        }
        
        list<TransAm_Job_History__c> jobrelatedList =getRelatedList ('TransAm_Job_History__c',appId,'TransAm_Application__c');
        TransAm_Job_History__c TransAmJH = new TransAm_Job_History__c();
        for(TransAm_Job_History__c oTransAmJobHistory : jobrelatedList ){
        TransAmJH = oTransAmJobHistory.clone();
        TransAmJH.TransAm_Application__c = cloneApp.id;
        TransAmJH.TransAm_EBE_Entry_ID__c = '';
         
        listToUpdate.Add(TransAmJH);
        }
        
        list<TransAm_Accident__c> accrelatedList =getRelatedList ('TransAm_Accident__c',appId,'TransAm_Application__c');
        TransAm_Accident__c TransAmAcc = new TransAm_Accident__c();
        for(TransAm_Accident__c oTransAmAcc : accrelatedList ){
        TransAmAcc = oTransAmAcc.clone();
        TransAmAcc.TransAm_Application__c = cloneApp.id;
        TransAmAcc.TransAm_EBE_Entry_ID__c = '';
         
        listToUpdate.Add(TransAmAcc);
        }
        
        list<TransAm_Felony__c> felonyList =getRelatedList ('TransAm_Felony__c',appId,'TransAm_Application__c');
        TransAm_Felony__c TransAmFelony = new TransAm_Felony__c();
        for(TransAm_Felony__c oTransAmFelony : felonyList ){
        TransAmFelony = oTransAmFelony.clone();
        TransAmFelony.TransAm_Application__c = cloneApp.id;
        TransAmFelony.TransAm_EBE_Entry_ID__c = '';
         
        listToUpdate.Add(TransAmFelony);
        }
        
        list<TransAm_License__c> licenseList =getRelatedList ('TransAm_License__c',appId,'TransAm_Application__c');
        TransAm_License__c TransAmLicense = new TransAm_License__c();
        for(TransAm_License__c oTransAmLicense : licenseList){
        TransAmLicense = oTransAmLicense.clone();
        TransAmLicense.TransAm_Application__c = cloneApp.id;
        TransAmLicense.TransAm_EBE_Entry_ID__c = '';
         
        listToUpdate.Add(TransAmLicense);
        }
        
        list<TransAm_Misdemeanor__c> misdemList =getRelatedList ('TransAm_Misdemeanor__c',appId,'TransAm_Application__c');
        TransAm_Misdemeanor__c TransAmMisdem = new TransAm_Misdemeanor__c();
        for(TransAm_Misdemeanor__c oTransAmMisdem : misdemList){
        TransAmMisdem = oTransAmMisdem.clone();
        TransAmMisdem.TransAm_Application__c = cloneApp.id;
        TransAmMisdem.TransAm_EBE_Entry_ID__c = '';
         
        listToUpdate.Add(TransAmMisdem);
        }
        
        list<TransAm_Violation__c> violationList =getRelatedList ('TransAm_Violation__c',appId,'TransAm_Application__c');
        TransAm_Violation__c TransAmViolation = new TransAm_Violation__c();
        for(TransAm_Violation__c oTransAmviolation : violationList){
        TransAmViolation = oTransAmviolation.clone();
        TransAmViolation.TransAm_Application__c = cloneApp.id;
        TransAmViolation.TransAm_EBE_Entry_ID__c = '';
         
        listToUpdate.Add(TransAmViolation);
        }
        
        
        system.debug('cloneApp.id+++'+cloneApp.id);
        try{
            insert listToUpdate;
        }catch(exception e){
            TransAm_Error_Log__c el = new TransAm_Error_Log__c();
            el.TransAm_Class_Name__c = 'TransAm_DriverConversion';
            el.TransAm_Method_Name__c = 'cloneApplication';
            el.TransAm_Module_Name__c = 'Application';
            el.TransAm_Exception_Message__c = e.getMessage();
            insert el;
            }
        system.debug('cloneApp.id+++'+cloneApp.id);
        return cloneApp.id;
        
    }

    public static string  getFields( String sApi ){
        if(sApi !=''){
        String sQuery = '';
        SObjectType objType = Schema.getGlobalDescribe().get(sApi);      
        Map<string,Schema.SObjectField> mfield = objType.getDescribe().fields.getMap();
        System.debug('inserting'+mfield);
        
        for(Schema.sObjectField field :mfield.values()){
        System.debug('field >>'+field );
            if(sQuery=='' && string.valueof(field)!='TransAm_DriverID__c')   
            sQuery = ' '+field;
        else if(string.valueof(field)!='TransAm_DriverID__c')
            sQuery = sQuery+','+field;
        }
        System.debug('sQuery>>'+sQuery);
        Return sQuery;
        }
        return '';

}

    public static List<Sobject> getRelatedList( String ObjectName, ID AppID, String lookupField){

        list<Sobject>  lstOfRelated = new list<Sobject>();
         
        String  sRelatedObject = getFields(ObjectName);
        String sQuery = 'Select '+sRelatedObject+' from '+ObjectName+ ' where '+lookupField+' = : appId';
        
        lstOfRelated = Database.Query(sQuery);
        
        return lstOfRelated;

}
    
}