/* Class Name   : TransAm_EventHandler
 * Description  : Handler class for the Event trigger
 * Created By   : Pankaj Singh
 * Created On   : 13-Feb-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh               13-Feb-2017              Initial version.
 *
 *****************************************************************************************/
public class TransAm_EventHandler{
    
    public static boolean prohibitBeforeInsertTrigger = false;
    public static boolean prohibitBeforeUpdateTrigger = false;
    public static boolean prohibitAfterInsertTrigger = false;
    public static boolean prohibitAfterDeleteTrigger = false;
    
    /****************************************************************************************
    * Created By      :  Pankaj Singh
    * Create Date     :  13-Feb-2017
    * Description     :  handler method for after insert event
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public static void afterInsertHandler(list<Event> eventList){
    
        if(prohibitAfterInsertTrigger){
            return;
        }        
        TransAm_EventHelper.populateVisitCountOnDriver(eventList);
        prohibitAfterInsertTrigger = true;
    }
    
     /****************************************************************************************
    * Created By      :  Pankaj Singh
    * Create Date     :  13-Feb-2017
    * Description     :  handler method for before insert event
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public static void beforeInsertHandler(list<Event> eventList){
    
        if(prohibitBeforeInsertTrigger){
            return;
        }        
        TransAm_EventHelper.populateVisitCountOnSchool(eventList);
        prohibitBeforeInsertTrigger = true;
    }
    
    /****************************************************************************************
    * Created By      :  Pankaj Singh
    * Create Date     :  13-Feb-2017
    * Description     :  handler method for before insert event
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public static void beforeUpdateHandler(list<Event> eventList){
    
        if(prohibitBeforeUpdateTrigger){
            return;
        }        
        TransAm_EventHelper.populateVisitCountOnSchool(eventList);
        prohibitBeforeUpdateTrigger = true;
    }
    
     /****************************************************************************************
    * Created By      :  Pankaj Singh
    * Create Date     :  21-Feb-2017
    * Description     :  handler method for before delete event
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public static void afterDeleteHandler(list<Event> eventList){
    
        if(prohibitAfterDeleteTrigger){
            return;
        }          
        TransAm_EventHelper.populateVisitCountOnDriver(eventList);
        //TransAm_EventHelper.populateVisitCountOnSchool(eventList);
        prohibitAfterDeleteTrigger = true;
    }

}