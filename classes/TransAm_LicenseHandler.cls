/* Class Name   : TransAm_LicenseHandler
 * Description  : Handler class for the License trigger
 * Created By   : Pankaj Singh
 * Created On   : 15-Feb-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh               15-Feb-2017              Initial version.
 *
 *****************************************************************************************/
public class TransAm_LicenseHandler{

    public static boolean prohibitBeforeInsertTrigger = false;
    public static boolean prohibitAfterInsertTrigger = false;
    public static boolean prohibitAfterUpdateTrigger = false;
    
    /****************************************************************************************
    * Created By      :  Pankaj Singh
    * Create Date     :  13-Feb-2017
    * Description     :  handler method for before insert event
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public static void beforeInsertHandler(list<TransAm_License__c> licenseList){
    
        if(prohibitBeforeInsertTrigger){
            return;
        }        
        TransAm_LicenseHelper.populateApplicationOnLicense(licenseList);
        prohibitBeforeInsertTrigger = true;
    }
     /****************************************************************************************
    * Created By      :  MJ
    * Create Date     :  04-April-2017
    * Description     :  handler method for after insert event
    * Modification Log:  Second version.
    ***************************************************************************************/
    public static void afterInsertHandler(list<TransAm_License__c> licenseList){
    
        if(prohibitAfterInsertTrigger){
            return;
        }        
        TransAm_LicenseHelper.updateExpirationdateOnApplication(licenseList);                
        prohibitAfterInsertTrigger = true;
    }
    
     /****************************************************************************************
    * Created By      :  MJ
    * Create Date     :  04-April-2017
    * Description     :  handler method for after Update event
    * Modification Log:  Second version.
    ***************************************************************************************/
    public static void afterUpdateHandler(list<TransAm_License__c> licenseList){
    
        if(prohibitAfterUpdateTrigger){
            return;
        }        
        TransAm_LicenseHelper.updateExpirationdateOnApplication(licenseList);                        
        prohibitAfterUpdateTrigger = true;
    }

}