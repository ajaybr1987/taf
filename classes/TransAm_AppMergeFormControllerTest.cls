/* CLass Name   : TransAm_AppMergeFormControllerTest
 * Description  : Test class for TransAm_AppMergeFormController
 * Created By   : Monalisa Das
 * Created On   : 11-May-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Monalisa Das                11-May-2017              Initial version.
 *  
 *
 *****************************************************************************************/
 @isTest(seeAllData=false)
public class TransAm_AppMergeFormControllerTest {
/************************************************************************************
* Method       :    setup
* Description  :    setup test data
* Parameter    :    NIL    
* Return Type  :    void
*************************************************************************************/
@testSetup static void setup() {
        Account acc = new Account();
        acc=TransAm_Test_DataUtility.createAccounts(1,true)[0];
        acc.TransAm_Driving_School_Code__c='C-2830';
        update acc;
        system.debug('account+++'+acc);
        Contact con = new Contact();
        con=TransAm_Test_DataUtility.createContacts(1,acc.id,'Applicant',true)[0];
        TransAm_Application__c masterApp =new TransAm_Application__c();
        masterApp = TransAm_Test_DataUtility.createApplications(1, 'Independent', 'Application Received',con, True)[0];  
        masterApp.TransAm_Primary_Application__c=false;
        masterApp.TransAm_DriverID__c = '';
        masterApp.TransAm_EBE_Entry_ID__c = '';
        update masterApp;
		
		 TransAm_Application__c childApp =new TransAm_Application__c();
        childApp = TransAm_Test_DataUtility.createApplications(1, 'Dependant', 'Application Received',con, True)[0];  
        childApp.TransAm_Primary_Application__c=false;
        childApp.TransAm_DriverID__c = '';
        childApp.TransAm_EBE_Entry_ID__c = '';
		childApp.TransAm_Master_Application__c = masterApp.Id;
        update childApp;
		
		
	
    }
       /************************************************************************************
    * Method       :    mergeFieldsPageTest
    * Description  :    Test Method to create mergeFieldsPage
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void mergeFieldsPageTest(){
	
		Test.startTest();
		
		 try{
			  TransAm_Application__c masterApp = [SELECT ID, TransAm_Master_Application__c FROM TransAm_Application__c where TransAm_Application_Restriction__c='Independent' LIMIT 1];
			   TransAm_Application__c childApp = [SELECT ID, TransAm_Master_Application__c FROM TransAm_Application__c where TransAm_Application_Restriction__c='Dependant' LIMIT 1];
			ApexPages.StandardController sc3 = new ApexPages.StandardController(masterApp);
			TransAm_ApplicationMergeFormController  mergeApps = new TransAm_ApplicationMergeFormController(sc3);
			TransAm_ApplicationMergeFormController.appsWrapper appWrapper = new TransAm_ApplicationMergeFormController.appsWrapper(childApp);
			appWrapper.selected = true;
			//appWrapper.childApp = childApp;
			mergeApps.appsWrapperList.add(appWrapper);
			mergeApps.mergeFieldsPage();
			mergeApps.mergeAppsFieldsPage();
        }
        catch(Exception e){
			System.debug('Exception e ::'+e.getMessage() + e.getStackTraceString() + e );
        }
		
		Test.stopTest();
	}
}