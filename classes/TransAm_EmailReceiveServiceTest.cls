@isTest(seeAllData=false)
public class TransAm_EmailReceiveServiceTest {

    @testsetup
    static void setup() {
        
    }
        
    public static testmethod void testEmailReceive() {
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        
        email.subject = 'Equest';
        email.fromname = 'name';
        email.plainTextBody = 'COMPANY NAME:  Trans Am Trucking\nACCOUNT: 471922\nSUB: 4888\nORDERED BY: Shawna Simpson\nFIRST NAME: Liam\nLAST NAME: Adams\nPO#: AP-0323055\nSERVICE FROM: Chicago/IL\nDEPARTURE DATE: 0601\nDEPARTURE TIME:  1100AM\nSERVICE TO: Kansas City/MO\nPRICE: 123.00\nCONFIRMATION: 12345678';
        system.debug(LoggingLevel.ERROR, email.plainTextBody);
        env.fromaddress = 'commercial.sales@greyhound.com';
        TransAm_EmailReceiveService service = new TransAm_EmailReceiveService();
        service.handleInboundEmail(email, env);
    }
}