global class TransAm_EmailReceiveService implements Messaging.InboundEmailHandler {
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, 
                                                           Messaging.Inboundenvelope envelope) {
    	String body = email.plainTextBody;
        List<string> tokens = body.split(':');
        boolean foundApp = false;	
        string appName;
        for(string s : tokens) {
            if(foundApp)
                appName = s;
            if(s.equals('PO#'))
            	foundApp = true;
        }
        TransAm_Application__c app = [select TransAm_Greyhound_Confirmation_Number__c, TransAm_Transportation_Cost__c
                                     from TransAm_Application__c where Name = :appName];
        return new Messaging.InboundEmailResult();                                                       
    }

}