/******************************************************************************************
* Create By    :     Suresh M
* Create Date  :     03/14/2017
* Description  :     Test class for StatusUpdateBatch batch class and TransAm_ScheduleAppStatusUpdateBatch class.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * <Name>                             <Date>           Initial version.
*****************************************************************************************/
@isTest
private class TransAm_ScheduleAppStatusUpdateBatchTest{
     /*******************************************************************************************
    * Method        :   dataSetup
    * Description   :   Purpose of this method is to setup test data.
    * Parameter     :   null
    * Return Type   :   void
    ******************************************************************************************/
    public static @testSetup void dataSetup() {        
        
        Contact testCon = new Contact();
        
        testCon = TransAm_Test_DataUtility.createContacts(1,null,'Applicant', true)[0];
        
        list<TransAm_Application__c> appList = new list<TransAm_Application__c>();
        
        appList = TransAm_Test_DataUtility.createApplications(2, 'Independent Contractor Only', 'Application Received', testCon, false);
        
        for(TransAm_Application__c app : appList){
            app.TransAm_State__c = 'AR';        
            //app.TransAm_Applicant__c = testCon.id;
        }
        
        insert appList;
        
        //Insert custom setting
        TransAm_Status_duration__c durationCS = new TransAm_Status_duration__c();
        durationCS.name = 'Duration Value';
        durationCS.TransAm_Duration__c = 30;
        
        insert durationCS;
        
        TransAm_Application__c testApp = [select id, Name, TransAm_Status__c from TransAm_Application__c limit 1];
        testApp.TransAm_Status__c = 'Recruiter Review';
        
        update testApp;
        system.debug('****'+[select id, Name, TransAm_Status__c from TransAm_Application__c where TransAm_Status__c = 'Recruiter Review' limit 1]);
    } 

    /************************************************************************************
    * Method       :    testScheduleAppStatusBatch
    * Description  :    Test Method to test if the scheduler runs and updates the status fieldon Applications.
    * Parameter    :    Null
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testScheduleAppStatusBatch() {       
        
        TransAm_Application__c appToUpdate = [select id, Name, TransAm_Status__c from TransAm_Application__c where TransAm_State__c = 'AR' and TransAm_Status__c = 'Recruiter Review' limit 1];
        
        system.assertEquals(appToUpdate.TransAm_Status__c, 'Recruiter Review');
        
        appToUpdate.TransAm_Last_Status_Change__c = system.now()-31;
        update appToUpdate;
        
        TransAm_Application__c updatedApp = [select id, Name, TransAm_Status__c, TransAm_Last_Status_Change__c  from TransAm_Application__c where TransAm_State__c = 'AR' and TransAm_Status__c = 'Recruiter Review' limit 1];
        
        system.debug('**** present'+updatedApp);
        system.assertEquals(updatedApp.TransAm_Status__c, 'Recruiter Review');
        
        Test.startTest();
        TransAm_ScheduleAppStatusUpdateBatch applicationstatusUpdate = new TransAm_ScheduleAppStatusUpdateBatch();
        String scheduleTime = '0 0 23 * * ?';
        System.schedule('Test AppSchedule Batch', scheduleTime, applicationstatusUpdate);
        //TransAm_ApplicationStatusUpdateBatch btch = new TransAm_ApplicationStatusUpdateBatch();
        //database.executebatch(btch);
        Test.stopTest();
        
        /*TransAm_Application__c afterBatchupdateApp = [select id, Name, TransAm_Status__c from TransAm_Application__c where TransAm_Status__c = 'No Contact' limit 1];
        
        system.assertEquals(afterBatchupdateApp.TransAm_Status__c, 'No Contact');*/
    }
         
}