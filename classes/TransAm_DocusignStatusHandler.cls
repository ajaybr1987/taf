/* Class Name   : TransAm_DocusignStatusHandler
 * Description  : Handler class for the Event trigger
 * Created By   : Pankaj Singh
 * Created On   : 20-Mar-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh               20-Mar-2017              Initial version.
 *
 *****************************************************************************************/
public class TransAm_DocusignStatusHandler{
    
    public static boolean prohibitAfterUpdateTrigger = false;
    public static boolean prohibitAfterInsertTrigger = false;
    
    /****************************************************************************************
    * Created By      :  Pankaj Singh
    * Create Date     :  13-Feb-2017
    * Description     :  handler method for after insert event
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public static void afterInsertHandler(list<dsfs__DocuSign_Status__c> docStatusList){
    
        if(prohibitAfterInsertTrigger){
            return;
        }        
        TransAm_DocuSignStatusHelper.updateApplicationStatus(docStatusList);
        prohibitAfterInsertTrigger = true;
    }
    
    /****************************************************************************************
    * Created By      :  Pankaj Singh
    * Create Date     :  13-Feb-2017
    * Description     :  handler method for after update event
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public static void afterUpdateHandler(list<dsfs__DocuSign_Status__c> docStatusList){
    
        if(prohibitAfterUpdateTrigger){
            return;
        }        
        TransAm_DocuSignStatusHelper.updateApplicationStatus(docStatusList);
        prohibitAfterUpdateTrigger = true;
    }
}