/******************************************************************************************
* Create By    :    Pardeep Rao
* Create Date  :    03/06/2017
* Description  :    Handler class for TransAm_AccountTrigger trigger.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * <Name>                             <Date>           Initial version.
*****************************************************************************************/

public class TransAm_AccountHandler{
   
     
   /*******************************************************************************************        
   * Method        :   beforeInsertHandler
   * Description   :   Purpose of this method is to call insertPrivateSchoolRecruiterNotes method 
   *                 from TransAm_AccountHelper class.
   * Parameter     :   list<account>
   * Return Type   :   void
   ******************************************************************************************/
    public static void afterInsertHandler(list<account> newAccountList){
           
        TransAm_AccountHelper.insertPrivateRecruiterComment(newAccountList);   
      
    }
   
       
}