public class TransAm_GetWeblinkJob implements Queueable, Database.AllowsCallouts{

    public final String orderId;
    public final String request;
    
    public TransAm_GetWeblinkJob(String orderId, String soapRequest) {
        this.orderId = orderId;
        this.request = soapRequest;
    }
    
    public void execute(QueueableContext context) {
        try{
            Http h = new Http();
            HttpRequest req = new HttpRequest(); 
            HttpResponse res = new HttpResponse();   
            TransAm_HireRight_Orders__c order = [SELECT TransAm_Application__c,TransAm_HireRight_Reports__c,TransAm_Report_Type__c,TransAm_Hireright_Order_ID__c, TransAm_Application__r.Name from TransAm_HireRight_Orders__c WHERE Name = :orderId];
            
            Integer ContentLength = 0; 
            ContentLength = request.length();
            req.setMethod('POST'); 
            req.setEndPoint('https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2'); 
            //req.setEndPoint('https://ows01.hireright.com/ws_gateway_stub/HireRightAPI/v/1/2'); 
            req.setHeader('Content-type','text/xml'); 
            req.setHeader('Content-Length',ContentLength.format()); 
            req.setHeader('SoapAction','GenerateWebLink');  
            req.setBody(request);
            System.debug('Req in sendRawRequestForWebLink::'+request);
            res = h.send(req); 
            System.Debug('res:'+res); 
            String auth = res.getBody(); 
            // System.Debug('Debug(auth:'+auth);
            
            String envNS = 'http://schemas.xmlsoap.org/soap/envelope/';
            String hrNS = 'urn:enterprise.soap.hireright.com/objs';        
            if(res.getStatusCode() == 200) {
                Dom.Document doc = res.getBodyDocument();
                Dom.XmlNode soapEnv = doc.getRootElement();
                Dom.XmlNode node = soapEnv.getChildElement('Body', envNS);
                node = node.getChildElement('GenerateWebLinkResponse', hrNS);
                node = node.getChildElement('Result', hrNS);
                node = node.getChildElement('ReportURL', hrNS);
                order.TransAm_HireRight_Reports__c = node.getText();
				order.TransAm_IS_Manual_Update__c = true;
                System.debug('order.TransAm_HireRight_Reports__c ::'+order.TransAm_HireRight_Reports__c);
                //TransAm_HireRightHandler.prohibitAfterUpdateTrigger = true;
                //System.debug('Calling download attachment');
                //downloadAttachment(order.TransAm_Application__c,order.TransAm_HireRight_Reports__c,order);
                //Database.update(order);
                //downloadAttachment(order.TransAm_Application__c,order.TransAm_HireRight_Reports__c,order.Id);
                //TransAm_HireRightHandler.prohibitAfterUpdateTrigger = false;
                
                System.enqueueJob(new TransAm_DownloadAttachmentJob(order));
            }
        }
        catch(Exception e)
        {
            System.debug('Exception ::'+e.getStackTraceString() + e.getMessage() +e);
            INSERT (new TransAm_Error_Log__c(TransAm_Class_Name__c = 'TransAm_GetWeblinkJob',TransAm_Method_Name__c = 'execute',TransAm_Module_Name__c = 'HireRight',TransAm_Exception_Message__c = e.getMessage() + e.getStackTraceString()));
            throw e;
        }
    }
}