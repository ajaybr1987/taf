/* CLass Name   : TransAm_Violation_Test
 * Description  : Test class for Violation
 * Created By   : Pankaj Singh
 * Created On   : 22-Feb-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh                22-Feb-2017              Initial version.
 *
 *****************************************************************************************/
@isTest
private class TransAm_Violation_Test{
    /************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
        List<Account> lstAccounts = TransAm_Test_DataUtility.createAccounts(1, true);
        List<Contact> lstContacts = TransAm_Test_DataUtility.createContacts(1, lstAccounts[0].Id, 'Applicant', true);
        List<TransAm_Application__c> lstApplications = TransAm_Test_DataUtility.createApplications(1, 'Company Driver Only', 'Prospect', lstContacts[0], True);
        Profile p = [SELECT id,name FROM Profile where name = 'TransAm_Integration_User'];
        User user = new User();
        user.FirstName = 'Neha';
        user.LastName = 'Jain';
        user.Username = 'Nehdfghjs@deloitte.com';
        user.Email   = 'Nehajain@deloitte.com';
        user.Alias   = 'Neha';
        user.TimeZoneSidKey   = 'America/Los_Angeles';
        user.LocaleSidKey   = 'en_US';
        user.EmailEncodingKey   = 'UTF-8';
        user.ProfileId = p.Id;                               
        user.LanguageLocaleKey = 'en_US'; 
        user.TransAm_Exclude_from_Assignment__c = FALSE;  
        user.TransAm_Last_Application_Assigned__c = FALSE; 
        insert user;
    }

    /************************************************************************************
    * Method       :    testViolation
    * Description  :    Test Method to create Violation
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testViolation() {
        User user =[Select Id FROM User Where USername ='Nehdfghjs@deloitte.com' limit 1];
        System.runAs(user){
            List<TransAm_Application__c> lstApplications = [SELECT ID , TransAm_EBE_Entry_ID__c FROM TransAm_Application__c LIMIT 1];
            list<TransAm_Violation__c> violationList = TransAm_Test_DataUtility.createViolations(1, lstApplications[0], FALSE);
            Test.startTest();
                insert violationList;
            	UPDATE violationList;
            Test.stopTest();
        }
    }
    
    /************************************************************************************
    * Method       :    testViolationException
    * Description  :    Test Method to cover exception block
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testViolationException() {
    	User user =[Select Id FROM User Where USername ='Nehdfghjs@deloitte.com' limit 1];
        System.runAs(user){
            Test.startTest();
            try{
                TransAm_ViolationHelper.populateApplicationOnViolation(null);
            }catch(Exception e){}
            Test.stopTest();
        }
    }
}