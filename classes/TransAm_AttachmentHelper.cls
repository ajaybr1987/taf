/******************************************************************************************
* Created By   :     Pankaj SIngh
* Create Date  :     03/19/2017
* Description  :     Utility class to migrate docusign documents to Recruitment documents related list.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * Pankaj Singh                      03/19/2017          Initial version.
*****************************************************************************************/
public with sharing class TransAm_AttachmentHelper {
    public static void transferReceivedFaxes(List<Attachment> attachmentList){
        List<Id> lstAttachmentParents = new List<Id>();
        Map<Id,Attachment> mapRecFaxAttachments = new Map<Id,Attachment>();
        Map<Id,Id> mapRecFaxWithRecrDocDetails = new Map<Id,Id>();
        List<Attachment> lstAttachmentsToBeInserted = new List<Attachment>();
        for(Attachment attmt:attachmentList){
            if(attmt.ParentId.getSObjectType().getDescribe().getName() == 'efaxapp__Received_Fax__c'){
                lstAttachmentParents.add(attmt.ParentId);
                mapRecFaxAttachments.put(attmt.ParentId, attmt);
            }
        }

        for(efaxapp__Received_Fax__c recFax:[SELECT Id, efaxapp__Sent_Fax__c,efaxapp__Sent_Fax__r.TransAm_Recruitment_Document__c FROM efaxapp__Received_Fax__c WHERE Id In :lstAttachmentParents]){
            if(recFax.efaxapp__Sent_Fax__c != null){    
                mapRecFaxWithRecrDocDetails.put(recFax.efaxapp__Sent_Fax__r.TransAm_Recruitment_Document__c, recFax.Id);
            }
        }

        for(Id recrDocId:mapRecFaxWithRecrDocDetails.Keyset()){
            Attachment attachmentObj   = new Attachment();
            attachmentObj.Body         = mapRecFaxAttachments.get(mapRecFaxWithRecrDocDetails.get(recrDocId)).Body;
            attachmentObj.Description  = mapRecFaxAttachments.get(mapRecFaxWithRecrDocDetails.get(recrDocId)).Description;
            attachmentObj.Name         = mapRecFaxAttachments.get(mapRecFaxWithRecrDocDetails.get(recrDocId)).Name;
            attachmentObj.ParentId     = recrDocId;
            lstAttachmentsToBeInserted.add(attachmentObj);
        }
        if(lstAttachmentsToBeInserted.size() > 0)
            Database.insert(lstAttachmentsToBeInserted);
    }
    /****************************************************************************************
    * Created By      :  Pankaj Singh
    * Create Date     :  17-April-2017
    * Description     :  transfer attachments from docusign status to recruitment documents 
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public static void transferAttachements(list<Attachment> attachmentList){
    
        list<Attachment> attachmentsToBeInserted = new list<Attachment>();
        Set<Id> docusignStatusIds = new set<Id>();
        map<Id,Id> statusApplicationMap = new map<Id,Id>();
        map<Id,Id> applicationDocumentMap = new map<Id,Id>();
        map<Id,list<TransAm_Job_History__c>> jobHistoryMap = new map<Id,list<TransAm_Job_History__c>>();
        map<string,string> recruitmentDocumentMap = new map<string,string>();
        map<string,Id> documentTypeMap = new map<string,Id>();
        list<TransAm_Recruitment_Document_Mapping__mdt> rdMappingList = new list<TransAm_Recruitment_Document_Mapping__mdt>();
        for(TransAm_Recruitment_Document_Mapping__mdt rdMapping : [SELECT ID,DeveloperName,MasterLabel,TransAm_Document_Type__c FROM 
                                                                   TransAm_Recruitment_Document_Mapping__mdt]){
              recruitmentDocumentMap.put(rdMapping.MasterLabel,rdMapping.TransAm_Document_Type__c);                                                     
        }
        system.debug('printing rd map:::'+recruitmentDocumentMap);   
        for(Attachment attachmentObj : attachmentList){  
            system.debug('inside if::'+attachmentObj.ParentId.getsObjectType());       
            if(String.valueOf(attachmentObj.ParentId.getsObjectType()) == 'dsfs__DocuSign_Status__c'){
                docusignStatusIds.add(attachmentObj.ParentId);      
            }
        }
        system.debug('printing docusignStatusIds>>>'+docusignStatusIds);
        if(!docusignStatusIds.isEmpty()){
            for(dsfs__DocuSign_Status__c ds : [SELECT ID, TransAm_Application__c FROM dsfs__DocuSign_Status__c WHERE ID IN : docusignStatusIds]){                                               
                statusApplicationMap.put(ds.Id,ds.TransAm_Application__c);                                  
            }
        }
        system.debug('printing statusApplicationMap>>>'+statusApplicationMap);
        for(TransAm_Application__c appObj : [SELECT ID,(SELECT ID FROM Job_Histories__r) FROM TransAm_Application__c WHERE ID IN : statusApplicationMap.values()]){
            if(appObj.Job_Histories__r!=null && !appObj.Job_Histories__r.isEmpty())
                jobHistoryMap.put(appObj.Id,appObj.Job_Histories__r);    
        } 
        system.debug('printing job history map:::'+jobHistoryMap);                                                  
        for(TransAm_Recruitment_Document__c rd : [SELECT ID, Name, TransAm_Type__c,TransAm_Application__c FROM TransAm_Recruitment_Document__c WHERE TransAm_Application__c 
                                                  IN :statusApplicationMap.values() ORDER BY Createddate DESC]){     
            documentTypeMap.put(rd.TransAm_Type__c,rd.Id);
            if(rd.TransAm_Application__c!=null){
                applicationDocumentMap.put(rd.TransAm_Application__c,rd.Id);    
            }
        }
        system.debug('printing application document map>>>'+applicationDocumentMap);
        system.debug('printing document type map>>>'+documentTypeMap);
        for(Attachment attachtObj : attachmentList){
            if(attachtObj.Name!=null && String.valueOf(attachtObj.ParentId.getsObjectType()) == 'dsfs__DocuSign_Status__c'){
                system.debug('printing document map:::'+attachtObj.ParentId);
                system.debug('printing document map:::'+statusApplicationMap.get(attachtObj.ParentId));
                Attachment attachmentObj   = new Attachment();
                attachmentObj.Body         = attachtObj.Body;
                attachmentObj.Description  = attachtObj.Description;
                attachmentObj.Name         = attachtObj.Name;
                if(attachmentObj.Name.contains('PRINT ONLY') && documentTypeMap.containsKey('Print Only')){
                    attachmentObj.ParentId = documentTypeMap.get('Print Only');
                }
                else if(recruitmentDocumentMap.containsKey(attachtObj.Name.substringbefore('.')) && 
                        documentTypeMap.containsKey(recruitmentDocumentMap.get(attachtObj.Name.substringbefore('.')))){
                            system.debug('inside else if');
                            attachmentObj.ParentId = documentTypeMap.get(recruitmentDocumentMap.get(attachtObj.Name.substringbefore('.')));
                }
                system.debug('printing parent id::'+attachmentObj.ParentId);
                if(attachtObj.Name.substringbefore('.') == 'Past_Emp_Verification'){
                    system.debug('inside job history');
                    for(TransAm_Job_History__c jh  : jobHistoryMap.get(statusApplicationMap.get(attachtObj.ParentId))){
                        Attachment attachObj       = new Attachment();
                        attachObj.Body             = attachtObj.Body;
                        attachObj.Description      = attachtObj.Description;
                        attachObj.Name             = attachtObj.Name;
                        attachObj.ParentId         = jh.Id;
                        attachmentsToBeInserted.add(attachObj);
                    }
                }                
                attachmentsToBeInserted.add(attachmentObj);
             }
        }
        system.debug('printing attachment list>>>'+attachmentsToBeInserted);
        if(!attachmentList.isEmpty()){
            //prohibitBeforeInsertTrigger =  true;
           // Database.insert(attachmentsToBeInserted);
           insert attachmentsToBeInserted;
        }
    }
    
    public static void updateRecruitmentDocumentExportFlag(list<Attachment> attachmentList){
        List<Id> idList = new List<Id>();
        for(Attachment att : attachmentList){
             System.debug('att.Parent.Type --> '+att.ParentId.getSObjectType().getDescribe().getName());
            if(att.ParentId.getSObjectType().getDescribe().getName() == 'TransAM_Recruitment_Document__c'){
                System.debug('att.Parent.Type --> '+att.ParentId.getSObjectType().getDescribe().getName());
                idList.add(att.ParentId);
            }
        }
        List<TransAM_Recruitment_Document__c> recruitList = [Select Id, TransAm_Is_Exported__c FROM TransAM_Recruitment_Document__c WHERE ID IN: idList];
        for(TransAM_Recruitment_Document__c objR : recruitList){
            objR.TransAm_Is_Exported__c = false;
        }
        UPDATE recruitList;
    }

 /****************************************************************************************
    * Created By      :  Shruti Sinha
    * Create Date     :  20-April-2017
    * Description     :  Check the Extension 
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public static void CheckTheExtensionOfAttachment(List<Attachment> attachmentList){
     
        for(Attachment att:attachmentList){
        if(att.Name!= null)
           if(!(att.Name.endsWith('.doc')
              ||att.Name.endsWith('.docx')
              ||att.Name.endsWith('.pdf')
              ||att.Name.endsWith('.tif')
              ||att.Name.endsWith('.jpg')
              ||att.Name.endsWith('.jpeg')
              ||att.Name.endsWith('.png')
              ||att.Name.endsWith('.xml'))){                
                  att.addError(Label.TransAm_FileType);
              }
          }          
     }
    /****************************************************************************************
    * Created By      :  Raushan Anand
    * Create Date     :  04-May-2017
    * Description     :  Rename Attachment name 
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public static void renameAttachments(list<Attachment> attachmentList){
        Set<Id> recDocIds = new Set<Id>();
        Map<Id,String> appMap = new Map<Id,String>();
        Set<String> attNameSet=new Set<String>();
        List<Attachment> attList=new List<Attachment>();
        for(Attachment att : attachmentList){
            if(att.ParentId.getSObjectType().getDescribe().getName() == 'TransAm_Recruitment_Document__c'){
                recDocIds.add(att.ParentId);
            }
            
        }
        List<TransAm_Recruitment_Document__c> recDocList = [Select Id,TransAm_Application__r.Name FROM TransAm_Recruitment_Document__c WHERE Id IN: recDocIds];
        for(TransAm_Recruitment_Document__c recDocObj : recDocList){
            appMap.put(recDocObj.Id,recDocObj.TransAm_Application__r.Name);
        }
        for(Attachment att : attachmentList){
            att.Name = att.Name.Replace(' ','_');
            if(appMap.containsKey(att.ParentId))
                att.Name = appMap.get(att.ParentId)+'_'+att.Name;
            if(att.Name.length()>40){
                att.addError(Label.TransAm_Attachment_Length_Error_Msg);
            }
            else{
            attNameSet.add(att.Name);
            }
        }
        //fetch attachment with the same name 
        //attList=[select id, name from attachment where name IN :attNameSet];
        //attNameSet.clear();
        //check if we have found any attachment with name 
        //if(attList.size()>0){
            //fetch all the duplicate names in set
          //  for(Attachment attRecord:attList){
           //    attNameSet.add(attRecord.Name); 
           // }
            //iterate over the attachments being inserted and check if it has a duplicate
            // name then add error message to that record
           // for(Attachment attRecord:attachmentList){
            //    if(attNameSet.contains(attRecord.Name)){
            //      attRecord.addError(Label.TransAm_Attachment_Duplicate_Name_Error_Msg);  
            //    }
           // }
        //}
    }
    
     /****************************************************************************************
    * Created By      :  Pranjal Singh
    * Create Date     :  23-May-2017
    * Description     :  Email Recruitment document to application owner 
    * Modification Log:  Initial version.
    ***************************************************************************************/
    /* public static void emailAttachmentToApplicationOwner(list<Attachment> attachmentList){
        Map<ID,TransAm_Recruitment_Document__c> RecDocMap=new Map<ID,TransAm_Recruitment_Document__c>();
        Set<Id> recDocIdSet=new Set<Id>();
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
       
        // To fetch all the recruitment document Ids
        for(Attachment attachmentRecord:attachmentList){
            if(attachmentRecord.parentId.getSObjectType().getDescribe().getName()=='TransAm_Recruitment_Document__c'){
               recDocIdSet.add(attachmentRecord.parentId);
            }
        }
        if(recDocIdSet.size()>0){
             RecDocMap=new Map<ID, TransAm_Recruitment_Document__c>([select ID, TransAm_Application__r.owner.email from TransAm_Recruitment_Document__c where id IN :recDocIdSet]);
             //setup all the emails to be sent
             for(Attachment attachmentRecord:attachmentList){
                 if(recDocIdSet.contains(attachmentRecord.parentId) && RecDocMap.get(attachmentRecord.parentId)!=null && RecDocMap.get(attachmentRecord.parentId).TransAm_Application__r.owner.email!=''){
                   Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                   String emailAddress=RecDocMap.get(attachmentRecord.parentId).TransAm_Application__r.owner.email;
                    // mail.setToAddresses(new List<String>{(RecDocMap.get(attachmentRecord.parentId)).TransAm_Application__r.owner.email});
                     mail.setToAddresses(new List<String>{'pkumarsingh@DELOITTE.com'});
                     mail.setSubject('Attachment Added!!');
                     Messaging.Emailfileattachment mailAttachment = new Messaging.Emailfileattachment();
                     mailAttachment.setFileName(attachmentRecord.Name);
                     mailAttachment.setBody(attachmentRecord.Body);
                     mail.setPlainTextBody( 'Mila kya attachment??' );
                     mail.setFileAttachments(new List<Messaging.Emailfileattachment>{mailAttachment});
                     mails.add(mail);
                     }
             }
            if(mails.size()>0){
                Messaging.SendEmailResult [] r = Messaging.sendEmail(mails);
            }
        }
    } */
}