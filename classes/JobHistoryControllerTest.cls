/* CLass Name   : JobHistoryControllerTest
 * Description  : Test class for controller JobHistoryController
 * Created By   : Neha Jain
 * Created On   : 30-Mar-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Neha Jain                28-Mar-2017              Initial version.
 *
 *****************************************************************************************/
@isTest
public class JobHistoryControllerTest{
    
    /************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
        List<Account> lstAccounts = TransAm_Test_DataUtility.createAccounts(1, true);
        List<Contact> lstContacts = TransAm_Test_DataUtility.createContacts(1, lstAccounts[0].Id, 'Applicant', true);
        TransAm_Application__c objApp = new TransAm_Application__c();
        objApp= TransAm_Test_DataUtility.createApplications(1, 'Independent Contractor Only', 'Application Received', lstContacts[0], True)[0];
        TransAm_Employer__c objEmp =new TransAm_Employer__c(Name='Neha',TransAm_City__c='Bangalore');
        insert objEmp;
        TransAm_Job_History__c objJobHistory = new TransAm_Job_History__c(TransAm_Employed_From__c=Date.newInstance(2017,3,13),
                                                TransAm_Employed_To__c=Date.newInstance(2017,3,28),TransAm_Application__c=objApp.id,
                                                TransAm_Employer2__c=objEmp.id,TransAm_Current_Employer__c='TRUE');                                  
        insert objJobHistory;
        
        TransAm_Job_History__c objJobHistory2 = new TransAm_Job_History__c(TransAm_Employed_From__c=Date.newInstance(2017,3,13),
                                                TransAm_Employed_To__c=Date.newInstance(2017,3,28),TransAm_Application__c=objApp.id,
                                                TransAm_Other_Employer__c='Deloitte');
        insert objJobHistory2;
        
        
     }

     /************************************************************************************
    * Method       :    testEmployerAssignment
    * Description  :    Test Method to create Job history
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testEmployerAssignment(){
    
        Test.startTest();
        TransAm_Job_History__c objJobHistory = [SELECT ID,TransAm_Employer2__c  FROM TransAm_Job_History__c where TransAm_Current_Employer__c='TRUE' LIMIT 1];
        ApexPages.StandardController sc = new ApexPages.StandardController(objJobHistory);
        JobHistoryController objJobs = new JobHistoryController(sc);
        objJobs.save();
        objJobs.saveNew();
        objJobs.cancel();
        objJobs.addressInfo();
        system.assertEquals(objJobHistory.TransAm_City__c, 'Bangalore');
        
        TransAm_Job_History__c objJobHistory2 = [SELECT ID  FROM TransAm_Job_History__c where TransAm_Other_Employer__c='Deloitte' LIMIT 1];
        ApexPages.StandardController sc2 = new ApexPages.StandardController(objJobHistory2);
        JobHistoryController objJobs2 = new JobHistoryController(sc2);
        objJobs2.otherEmployer();
        
        try{
            ApexPages.StandardController sc3 = new ApexPages.StandardController(objJobHistory2);
            JobHistoryController objJobs3 = new JobHistoryController(sc3);
            objJobs3.recordId= objJobHistory2.id;
        }
        catch(Exception e){
            Test.stopTest();
        }
    }    
}