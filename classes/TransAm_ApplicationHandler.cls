/******************************************************************************************
* Create By    :     Suresh M
* Create Date  :     02/17/2017
* Description  :     Handler class for TransAm_ApplicationTrigger trigger.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * <Name>                             <Date>           Initial version.
*****************************************************************************************/
public class TransAm_ApplicationHandler{

    Private Static Integer count=0;
    public static boolean prohibitAfterInsertTrigger = false;
    public static boolean prohibitBeforeInsertTrigger = false;
    public static boolean prohibitBeforeUpdateTrigger = false;
    public static boolean prohibitAfterUpdateTrigger = false;
    public static boolean prohibitAfterDeleteTrigger = false;
    
   /*******************************************************************************************        
   * Method        :   beforeInsertHandler
   * Description   :   Purpose of this method is to call updateApplicationStatus method from TransAm_ApplicationHelper class.
   * Parameter     :   List<TransAm_Application__c>
   * Return Type   :   Void
   ******************************************************************************************/
    public static void beforeInsertHandler(list<TransAm_Application__c> newApplicationList,Map<id, TransAm_Application__c> newApplicationMap, Boolean isBeforeInsert){
        
        if(prohibitBeforeInsertTrigger){
            return;
        }
        
        prohibitBeforeInsertTrigger = true;
        
        for(TransAm_Application__c newApp : newApplicationList){
            newApp.TransAm_Status__c = 'Application Received';
        }
        
        TransAm_ApplicationHelper.updateApplicationStatus(newApplicationList, null, null, isBeforeInsert, false);   
        TransAm_ApplicationHelper.validateApplicantStatus(newApplicationList,newApplicationMap);
        system.debug('printing user id::'+UserInfo.getUserId());
        system.debug('printing integration user id::'+[SELECT ID FROM User WHERE Profile.Name = 'TransAm_Integration_User' LIMIT 1].Id);
        Id profileId=userinfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        if(profileName == 'TransAm_Integration_User'){
            TransAm_ApplicationAssignmentHelper.assignApplication(newApplicationList);
        }
        //TransAm_ApplicationHelper.recruiterFieldsUpdate(newApplicationList, isBeforeInsert, false);
        TransAm_ApplicationHelper.calculateScoreonApplication(newApplicationList);
        TransAm_ApplicationHelper.updateDQStatusAndReason(newApplicationList);
        TransAm_ApplicationHelper.emailPhoneSSNCheck(newApplicationList);
        TransAm_ApplicationHelper.populateKeyFieldsOnApplicationBeforeInsert(newApplicationList);
   	TransAm_ApplicationHelper.updateArchivalDate(newApplicationList);
    }
    
   /*******************************************************************************************
   * Method        :   beforeUpdateHandler
   * Description   :   Purpose of this method is to call updateApplicationStatus method from TransAm_ApplicationHelper class.
   * Parameter     :   List<TransAm_Application__c>
   * Return Type   :   Void
   ******************************************************************************************/
    public static void beforeUpdateHandler(list<TransAm_Application__c> newApplicationList, Map<id, TransAm_Application__c> newApplicationMap, Map<id, TransAm_Application__c> oldApplicationMap, boolean isBeforeUpdate){
        
        if(prohibitBeforeUpdateTrigger){
            return;
        }
        
        prohibitBeforeUpdateTrigger = true;
        
        TransAm_ApplicationHelper.updateApplicationStatus(newApplicationList, newApplicationMap, oldApplicationMap, false, isBeforeUpdate);        
        TransAm_ApplicationHelper.validateApplicantStatus(newApplicationList,newApplicationMap);
        TransAm_ApplicationHelper.updateApplicationOwner(newApplicationList, oldApplicationMap,newApplicationMap); 
        TransAm_ApplicationHelper.updateEmpStatus(newApplicationList, oldApplicationMap);
        TransAm_ApplicationHelper.calculateScoreonApplication(newApplicationList);
        TransAm_ApplicationHelper.updateDQStatusAndReason(newApplicationList);
        TransAm_ApplicationHelper.updateLastName(newApplicationList);
        TransAm_ApplicationHelper.updateConRecTypeToDriverForDriverCode(newApplicationList, oldApplicationMap);
        TransAm_ApplicationHelper.populateKeyFieldsOnApplicationBeforeUpdate(newApplicationList, oldApplicationMap);
        TransAm_ApplicationHelper.preventMultipleMasterApps(newApplicationList, newApplicationMap);
        TransAm_ApplicationHelper.updateArchivalDate(newApplicationList);
    }
    
    
   /*******************************************************************************************
   * Method        :   afterUpdateHandler
   * Description   :   Purpose of this method is to call updateApplicationOwner method from TransAm_ApplicationHelper class.
   * Parameter     :   List<TransAm_Application__c>
   * Return Type   :   Void
   ******************************************************************************************/
    public static void afterUpdateHandler(list<TransAm_Application__c> newApplicationList, Map<id, TransAm_Application__c> oldApplicationMap, boolean isafterUpdate){     
       if(prohibitAfterUpdateTrigger ){
            return;
        }
        
        prohibitAfterUpdateTrigger = true;
        TransAm_ApplicationHelper.updateContactFromPrimaryApp(newApplicationList, oldApplicationMap);
        //TransAm_ApplicationHelper.updateContactFromApplication(newApplicationList, oldApplicationMap);
        TransAm_ApplicationHelper.updateRecruitmentDocuments(newApplicationList, oldApplicationMap, isafterUpdate);
        TransAm_ApplicationHelper.countChildRecords(newApplicationList);
        TransAm_ApplicationHelper.updateTerminationDate(newApplicationList, oldApplicationMap);
        TransAm_ApplicationHelper.updateConRecTypeOnAssociatingApp(newApplicationList, oldApplicationMap);
    } 
 /*******************************************************************************************
   * Method        :   afterInsertHandler
   * Description   :   Purpose of this method is to call helper methods from TransAm_ApplicationHelper class.
   * Parameter     :   List<TransAm_Application__c>, Map<id, TransAm_Application__c>, boolean
   * Return Type   :   Void
   ******************************************************************************************/
    public static void afterInsertHandler(list<TransAm_Application__c> newApplicationList, Map<id, TransAm_Application__c> newApplicationMap, boolean isAfterInsert){
        
        if(prohibitAfterInsertTrigger){
            if(count>0){
                TransAm_ApplicationHelper.childAppCreationIndication(newApplicationList);
                TransAm_ApplicationHelper.countChildRecords(newApplicationList);
            }
            return;
        }
        
        prohibitAfterInsertTrigger = true;
        count++;
        system.debug('##calling helper');
        TransAm_ApplicationHelper.searchAndLinkAppContacts(newApplicationList, newApplicationMap, isAfterInsert);
        TransAm_ApplicationHelper.createRecruitmentDocuments(newApplicationList, isAfterInsert);
        TransAm_ApplicationHelper.childAppCreationIndication(newApplicationList);
        TransAm_ApplicationHelper.countChildRecords(newApplicationList);
    }
    
    public static void afterDeleteHandler(list<TransAm_Application__c> oldApplicationList, Map<id, TransAm_Application__c> oldApplicationMap, boolean isAfterDelete){
    
        if(prohibitAfterDeleteTrigger){
            return;
        }
        prohibitAfterDeleteTrigger = true;
        TransAm_ApplicationHelper.countChildRecords(oldApplicationList);
    }        
}