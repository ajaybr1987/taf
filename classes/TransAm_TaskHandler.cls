public class TransAm_TaskHandler{

    public static boolean prohibitAfterInsertTrigger = false;

    public static void afterInsertHandler(list<Task> taskList){
    
        if(prohibitAfterInsertTrigger){
            return;
        }        
        TransAm_TaskHelper.populateVisitCountOnSchool(taskList);
        prohibitAfterInsertTrigger = true;
    }

}