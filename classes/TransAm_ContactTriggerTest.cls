/******************************************************************************************
* Create By    :     Suresh M
* Create Date  :     02/15/2017
* Description  :     This test class ensures code coverage for TransAm_ContactTrigger trigger , TransAm_ContactHandler class and TransAm_ContactHelper class.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * <Name>                             <Date>           Initial version.
*****************************************************************************************/
@isTest(seeAllData=false)
public class TransAm_ContactTriggerTest{    
    
    //Method to test before insert event functionality
    public static testmethod void beforeInsertTest(){
                        
        Id conRecTypeId = [Select Name, Id From RecordType where sObjectType='Contact' and developerName = 'TransAm_School_Contact' and isActive=true].id;        
        
        Account acc = [select id, name from account where TransAm_Driving_School_Code__c = '123'];                
        
        list<contact> conInsetedList = [select id, name, TransAm_Main_Contact__c from contact where TransAm_Main_Contact__c = true and name like 'TestConInsert%'];
        
        system.assertEquals(1,conInsetedList.size());
        
        contact newContact = new contact();
        newContact.lastName = 'mylast';   
        newContact.FirstName = 'myfirst';
        newContact.TransAm_Main_Contact__c = true;
        newContact.accountid = acc.id;
        newContact.recordtypeid = conRecTypeId;
        newContact.Email = 'liam5557@gmail.com';
        newContact.MobilePhone = '1234567891';
        newContact.Phone = '1234567891';
        newContact.TransAm_SSN__c = '123456789';
        insert newContact;      
        
        list<contact> conListChecked = [select id, TransAm_FirstNameLastNameCellPhone__c, name,TransAm_Main_Contact__c from contact where TransAm_Main_Contact__c = true and lastname = 'mylast'];        
        system.assertEquals(1, conListchecked.size());

        list<contact> conListUnchecked = [select id, name,TransAm_Main_Contact__c from contact where TransAm_Main_Contact__c = false and name like 'TestConInsert%'];        
        system.assertEquals(2, conListUnchecked.size());
        
        contact test = conListChecked.get(0);
        system.assertEquals(test.TransAm_FirstNameLastNameCellPhone__c, 'myfirstmylast1234567891');
        
        boolean exceptionThrown = true;
        list<contact> contacts = new list<contact>();
        TransAm_ContactHandler.prohibitBeforeInsertTrigger = false;
        newContact = new contact();
        newContact.LastName = 'test1';
        newContact.TransAm_SSN__c = '123456789';
        
        try {
        	exceptionThrown = true;
        	insert newContact;
            exceptionThrown = false;
        } catch(Exception e){
            system.debug(LoggingLevel.ERROR, 'exception thrown ' + e.getMessage());
            System.AssertEquals(exceptionThrown, true);
        }
        
        contacts.clear();
        TransAm_ContactHandler.prohibitBeforeInsertTrigger = false;
        newContact = new contact();
        newContact.Email = 'liam5557@gmail.com';
        newContact.LastName = 'mylast';
        newContact.TransAm_SSN__c = '123456782';
        try {
        	exceptionThrown = true;
        	insert newContact;
            exceptionThrown = false;
        } catch(Exception e){
            system.debug(LoggingLevel.ERROR, 'exception thrown ' + e.getMessage());
            System.AssertEquals(exceptionThrown, true);
        }
        
        contacts.clear();
        TransAm_ContactHandler.prohibitBeforeInsertTrigger = false;
        newContact = new contact();
        newContact.FirstName = 'myfirst';
        newContact.LastName = 'mylast';
        newContact.TransAm_SSN__c = '123456785';
        newContact.MobilePhone = '1234567891';
        try {
            exceptionThrown = true;
        	insert newContact;
            exceptionThrown = false;
        } catch(Exception e){
            system.debug(LoggingLevel.ERROR, 'exception thrown ' + e.getMessage());
            System.AssertEquals(exceptionThrown, true);
        }
        
        contacts.clear();
        TransAm_ContactHandler.prohibitBeforeInsertTrigger = false;
        newContact = new contact();
        newContact.FirstName = 'myfirst';
        newContact.LastName = 'mylast';
        newContact.TransAm_SSN__c = '123456787';
        newContact.Phone = '1234567891';
        try {
            exceptionThrown = true;
        	insert newContact;
            exceptionThrown = false;
        } catch(Exception e){
            system.debug(LoggingLevel.ERROR, 'exception thrown ' + e.getMessage());
            System.AssertEquals(exceptionThrown, true);
        }
    }
    
    //Method to test before update event functionality
    public static testmethod void beforeUpdateTest(){                                
        system.debug(LoggingLevel.ERROR, 'before update test');                
        list<contact> insertedConUnchk = [select id, name, TransAm_Main_Contact__c from contact where TransAm_Main_Contact__c = false and name like 'beforeUpdateCon%'];
        system.assertEquals(2, insertedConUnchk.size());
                
        list<contact> insertedConchk = [select id, name, TransAm_Main_Contact__c from contact where TransAm_Main_Contact__c = true and lastname = 'beforeUpdateCon'];
        system.assertEquals(1, insertedConchk.size());
        
        Contact conToUpdate = insertedConUnchk.get(0);
        conToUpdate.TransAm_Main_Contact__c = true;
        Update conToUpdate;
        
        list<contact> newConAfterUpdatelist1 = [select id, name, TransAm_SSN__c, MobilePhone, Phone, TransAm_Main_Contact__c from contact where TransAm_Main_Contact__c = false and name like 'beforeUpdateCon%'];
        system.assertEquals(2, newConAfterUpdatelist1.size());
        
        list<contact> newConAfterUpdatelist2 = [select id, name, TransAm_Main_Contact__c from contact where TransAm_Main_Contact__c = true and name like 'beforeUpdateCon%'];
        system.assertEquals(1, newConAfterUpdatelist2.size());
        system.assertEquals(conToUpdate.name, newConAfterUpdatelist2.get(0).name);
        
        //conToUpdate = newConAfterUpdatelist2.get(0);
        TransAm_ContactHandler.prohibitBeforeUpdateTrigger = false;
        conToUpdate.LastName = 'new6887';
        conToUpdate.FirstName = 'newfirst';
        conToUpdate.Phone = '123456789';
        conToUpdate.MobilePhone = '1234567891';
        Update conToUpdate;
        list<contact> newConAfterUpdatelist3 = [select id, FirstName, TransAm_FirstNameLastNamePhone__c, TransAm_FirstNameLastNameCellPhone__c from contact where firstname = 'newfirst' and lastName = 'new6887'];
        conToUpdate = newConAfterUpdatelist3.get(0);
        system.debug(LoggingLevel.ERROR, 'firstlastphone ' + conToUpdate.TransAm_FirstNameLastNamePhone__c);
        system.assertEquals(conToUpdate.TransAm_FirstNameLastNamePhone__c, 'newfirstnew6887123456789');
        system.assertEquals(conToUpdate.TransAm_FirstNameLastNameCellPhone__c, 'newfirstnew68871234567891');
        
        
        TransAm_ContactHandler.prohibitBeforeUpdateTrigger = false;
        system.debug(LoggingLevel.ERROR, 'list size ' + newConAfterUpdatelist1.size());
        contact con1 = newConAfterUpdatelist1.get(0);
        contact con2 = newConAfterUpdatelist1.get(1);
        boolean exceptionThrown = true;
        con2.TransAm_SSN__c = con1.TransAm_SSN__c;
        try {
        	update con2;    
            exceptionThrown = true;
        } catch(Exception e) {
            System.AssertEquals(exceptionThrown, true);
        }
        
        exceptionThrown = false;
        TransAm_ContactHandler.prohibitBeforeUpdateTrigger = false;
        con2.TransAm_SSN__c = '235456564';
        con1.Email = 'liam87@gmail.com';
        update con1;
        try {
            con2.Email = 'liam87@gmail.com';
            update con2;
        } catch(Exception e) {
            system.debug(LoggingLevel.ERROR, 'exception thrown ' + e.getMessage());
            System.AssertEquals(exceptionThrown, true);
        }
        
        exceptionThrown = false;
        TransAm_ContactHandler.prohibitBeforeUpdateTrigger = false;
        con1.Email = 'sdf@gmail.com';
        con1.MobilePhone = '1234567891';
        con1.FirstName = 'john';
        con1.LastName = 'stevens';
        update con1;
        con2.MobilePhone = '1234567891';
        con2.FirstName = 'john';
        con2.LastName = 'stevens';
        try {
            exceptionThrown = true;
            update con2;
        } catch(Exception e) {
            system.debug(LoggingLevel.ERROR, 'exception thrown ' + e.getMessage());
            System.AssertEquals(exceptionThrown, true);
        }
        
        exceptionThrown = false;
        TransAm_ContactHandler.prohibitBeforeUpdateTrigger = false;
        con1.MobilePhone = '1234567895';
        con1.Phone = '1234567895';
        con1.FirstName = 'john';
        con1.LastName = 'stevens';
        update con1;
        con2.Phone = '1234567895';
        con2.FirstName = 'john';
        con2.LastName = 'stevens';
        try {
            exceptionThrown = true;
            update con2;
        } catch(Exception e) {
            system.debug(LoggingLevel.ERROR, 'exception thrown ' + e.getMessage());
            System.AssertEquals(exceptionThrown, true);
        }
    }
    
    
    //Method to test after undelete event functionality
    public static testmethod void afterUndeleteTest(){
        system.debug('see existing recs :'+[select id, name, TransAm_Main_Contact__c from contact where name like 'Test%']);
        Contact conToDelete = [select id, name, TransAm_Main_Contact__c from contact where TransAm_Main_Contact__c = true and name like 'Test%' limit 1];
        delete conToDelete;
        
        list<contact> conAfterDeleteList = [select id, name, TransAm_Main_Contact__c from contact where name like 'Test%'];
        
        system.assertEquals(1, conAfterDeleteList.size());
        
        undelete conToDelete;
        
        list<contact> conAfterUnDeleteTrueList = [select id, name, TransAm_Main_Contact__c from contact where TransAm_Main_Contact__c = true and name like 'Test%'];
        system.assertEquals(0, conAfterUnDeleteTrueList.size());
        
        list<contact> conAfterUnDeleteFalseList = [select id, name, TransAm_Main_Contact__c from contact where TransAm_Main_Contact__c = false and name like 'Test%'];
        system.assertEquals(2, conAfterUnDeleteFalseList.size());
    }
    
    public static @testSetup void Testsetupmethod(){
        
        //**Test data for beforeInsertTest and afterUndeleteTest methods**//
        list<contact> conToInsertList = new list<contact>();
        Id conRecTypeId = [Select Name, Id From RecordType where sObjectType='Contact' and developerName = 'TransAm_school_contact' and isActive=true].id;
        Id appRecTypeId = [Select Name, Id From RecordType where sObjectType='Contact' and developerName = 'TransAm_Applicant' and isActive=true].id;
        
        Account testAccount = new Account();
        testAccount.name = 'Test account';
        testAccount.TransAm_Driving_School_Code__c = '123';
        
        insert testAccount;
                
        contact contactToInsert1 = new contact();
        contactToInsert1.lastName = 'TestConInsert1';            
        contactToInsert1.TransAm_Main_Contact__c = false;
        contactToInsert1.accountid = testAccount.id;
        contactToInsert1.recordtypeid = conRecTypeId;
        contactToInsert1.TransAm_Driver_Code__c = '123';
        contactToInsert1.TransAm_School_Id__c = '111';
        contactToInsert1.TransAm_SSN__c='AAA';
        //contactToInsert1.TransAm_Phone__c='8888888888';
        //contactToInsert1.TransAm_Email__c='abc@gmail.com';
        contactToInsert1.FirstName = 'TestConInsert1first';  
        
        
        contact contactToInsert2 = new contact();
        contactToInsert2.lastName = 'TestConInsert2';            
        contactToInsert2.TransAm_Main_Contact__c = true;
        contactToInsert2.accountid = testAccount.id;
        contactToInsert2.recordtypeid = conRecTypeId;
        contactToInsert2.TransAm_Driver_Code__c = '123';
        contactToInsert2.TransAm_School_Id__c = '111';
        contactToInsert2.TransAm_SSN__c='bbb';
        //contactToInsert2='8888888888';
        //contactToInsert2='bcd@gmail.com';
        contactToInsert2.FirstName = 'TestConInsert2first';  
                
        conToInsertList.add(contactToInsert1);
        conToInsertList.add(contactToInsert2);
        
        insert conToInsertList;
        
        //**End of test data**//                              
            
        //Test data for beforeUpdateTest method
        
        list<contact> conToInsertList2 = new list<contact>();
                
        contact contactToInsert3 = new contact();
        contactToInsert3.lastName = 'beforeUpdateCon';            
        contactToInsert3.TransAm_Main_Contact__c = true;
        contactToInsert3.accountid = testAccount.id;
        contactToInsert3.recordtypeid = conRecTypeId;                        
        contactToInsert3.Phone='8888888888';
        contactToInsert3.Email='abc@gmail.com';
        contactToInsert3.TransAm_FirstNameLastNamePhone__c='beforeUpdateCon8888888888';
        contactToInsert3.TransAm_FirstNameLastNameEmail__c='beforeUpdateConabc@gmail.com';
        insert contactToInsert3;

        /*contactToInsert3.TransAm_SSN__c='236812681';
        contactToInsert3.Phone='8888888888';
        contactToInsert3.Email='abc@gmail.com';
        update contactToInsert3;*/
        
        for(integer i=0; i<2; i++){
            contact contactToInsert4 = new contact();
            contactToInsert4.lastName = 'beforeUpdateCon'+i;
            contactToInsert4.TransAm_Main_Contact__c = false;
            contactToInsert4.accountid = testAccount.id;
            contactToInsert4.recordtypeid = conRecTypeId;
            
            conToInsertList2.add(contactToInsert4);
        }
        insert conToInsertList2;

        /*contact contactToInsert5 = new contact();
        contactToInsert5.lastName = 'TestConInsert2';         
        contactToInsert5.TransAm_Main_Contact__c = true;
        contactToInsert5.accountid = testAccount.id;
        contactToInsert5.recordtypeid = appRecTypeId;                        
        
        insert contactToInsert5;
        contactToInsert5.TransAm_SSN__c='236812681';
        contactToInsert5.Phone='8888888888';
        contactToInsert5.Email='abc@gmail.com';
        contactToInsert5.TransAm_FirstNameLastNamePhone__c='TestConInsert28888888888';
        contactToInsert5.TransAm_FirstNameLastNameEmail__c='TestConInsertabc@gmail.com';
        update contactToInsert5; */   
    }
    
    public static testmethod void testDummyAccountPopulation(){
        
        //**Test data for beforeInsertTest and afterUndeleteTest methods**//
        String dummyAccountName = Label.TransAm_Dummy_Account_Name;
        Id appRecTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Applicant').getRecordTypeId();
        
        Account testAccount = new Account();
        testAccount.name = dummyAccountName + ' 01';
        testAccount.TransAm_Driving_School_Code__c = '7497946804';
        insert testAccount;
        
        contact contactToInsert = new contact();
        contactToInsert.lastName = 'TestConInsert5748';            
        contactToInsert.TransAm_Main_Contact__c = true;
        contactToInsert.recordtypeid = appRecTypeId;
        Test.startTest();
            insert contactToInsert;
        Test.stopTest();
    }
    
}