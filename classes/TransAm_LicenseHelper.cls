/* Class Name   : TransAm_License Helper
 * Description  : Helper class for the License trigger
 * Created By   : Pankaj Singh
 * Created On   : 15-Feb-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh               15-Feb-2017              Initial version.
 *
 *****************************************************************************************/
public class TransAm_LicenseHelper{

    /****************************************************************************************
    * Created By      :  Pankaj Singh
    * Create Date     :  15-Feb-2017
    * Description     :  populate the application Id on the License record
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public static void populateApplicationOnLicense(list<TransAm_License__c > licenseList){
    
        Id profileId = [SELECT ID FROM Profile WHERE Name = 'TransAm_Integration_User'].Id;
        if(UserInfo.getProfileId()==profileId){       
            map<string,Id> applicationMap = new map<string,Id>(); 
            set<String> parentIdSet = new set<String>();       
            try{
                for(TransAm_License__c licenseObj : licenseList){
                    parentIdSet.add(licenseObj.TransAm_EBE_Parent_ID__c);        
                }
                for(TransAm_Application__c applicationObj : [SELECT ID, TransAm_EBE_Entry_ID__c FROM TransAm_Application__c WHERE TransAm_EBE_Entry_ID__c IN : parentIdSet]){
                    applicationMap.put(applicationObj.TransAm_EBE_Entry_ID__c,applicationObj.Id);
                }
                for(TransAm_License__c licenseObj : licenseList){
                    if(applicationMap.containsKey(licenseObj.TransAm_EBE_Parent_ID__c)){
                        licenseObj.TransAm_Application__c = applicationMap.get(licenseObj.TransAm_EBE_Parent_ID__c);
                    }
                }
            }catch(Exception excep){
                TransAm_Error_Log__c el = new TransAm_Error_Log__c();
                el.TransAm_Class_Name__c = 'TransAm_LicenseHelper';
                el.TransAm_Method_Name__c = 'populateApplicationOnLicense';
                el.TransAm_Module_Name__c = 'License';
                el.TransAm_Exception_Message__c = excep.getMessage();
                insert el;
            }
        }
  }

   /****************************************************************************************
    * Created By      :  MJ
    * Create Date     :  04-April-2017
    * Description     :  populate the expiration date on application record
    * Modification Log:  Initial version.
    ***************************************************************************************/
  
    public static void updateExpirationdateOnApplication(list<TransAm_License__c> licenseList){
            
        set<Id> applicationIdSet =new set<Id>();
        map<Id,TransAm_License__c> applicationLicenseMap = new map<Id,TransAm_License__c>();
        List<TransAm_Application__c> listOfApplications =new List<TransAm_Application__c>();
        List<TransAm_Application__c> appsToBeUpdated =new List<TransAm_Application__c>();
        for(TransAm_License__c licenseObj : licenseList){
            applicationIdSet.add(licenseObj.TransAm_Application__c);
        } 
        if(applicationIdSet.size()>0){
            for(TransAm_Application__c applicationObj : [SELECT ID, TransAm_License_Expiration_Date__c ,Name,TransAm_State__c, (SELECT ID,TransAm_LicenseNo__c,TransAm_Expiration_Date__c,TransAm_State__c FROM Licenses__r 
                                                         WHERE TransAm_Current__c='Yes' AND TransAm_Expiration_Date__c!=null AND TransAm_Expiration_Date__c>TODAY ORDER BY TransAm_Expiration_Date__c DESC LIMIT 1) FROM TransAm_Application__c WHERE ID IN : applicationIdSet]){
                                                         
                listOfApplications.add(applicationObj);
                if(applicationObj.Licenses__r.size()>0)
                    applicationLicenseMap.put(applicationObj.Id,applicationObj.Licenses__r);                                            
            }
        }    
        for(TransAm_Application__c app : listOfApplications){
            if(applicationLicenseMap.containsKey(app.Id)){
                app.TransAm_License_Expiration_Date__c = applicationLicenseMap.get(app.Id).TransAm_Expiration_Date__c;
                app.TransAm_License_State__c           = applicationLicenseMap.get(app.Id).TransAm_State__c;
                app.TransAm_LicenseNo__c               = applicationLicenseMap.get(app.Id).TransAm_LicenseNo__c;
                appsToBeUpdated.add(app);
            }
        }
        if(!appsToBeUpdated.isEmpty()){
            Database.update(appsToBeUpdated); 
        }                
    }  
}