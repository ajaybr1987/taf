/******************************************************************************************
* Created By   :     Pankaj SIngh
* Create Date  :     03/19/2017
* Description  :     Utility class to migrate docusign documents to Recruitment documents related list.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * <Name>                             <Date>           Initial version.
*****************************************************************************************/

public class TransAm_MigrateDocuSignDocuments{

  /*******************************************************************************************
   * Method        :   transferDocumentsToRD
   * Description   :   Update application status based on the docusign status envelope status.
   * Parameter     :   list<dsfs__DocuSign_Status__c>
   * Return Type   :   void
   ******************************************************************************************/
   @future
   public static void transferDocumentsToRD(set<Id> docuSignStatusIdList){
       
       set<Id> applicationIds = new set<Id>();       
       list<Attachment> attachmentList = new list<Attachment>();
       map<Id,list<Attachment>> attachmentMap = new map<Id,list<Attachment>>(); 
       map<Id,Id> applicationDocStatusMap = new map<Id,Id>();
       map<Id,Id> applicationDocumentMap = new map<Id,Id>();
       list<dsfs__DocuSign_Status__c> docuSignStatusList = new list<dsfs__DocuSign_Status__c>();
       list<Attachment> attachList = [SELECT ID FROM Attachment where parentId IN : docuSignStatusIdList];
       
       docuSignStatusList = [SELECT ID,TransAm_Application__c,(SELECT ID FROM Attachments) FROM dsfs__DocuSign_Status__c WHERE ID IN : docuSignStatusIdList];                                          
       
       for(dsfs__DocuSign_Status__c ds : docuSignStatusList){                
           applicationDocStatusMap.put(ds.Id,ds.TransAm_Application__c);
           attachmentMap.put(ds.Id,ds.Attachments);
           applicationIds.add(ds.TransAm_Application__c);
       }
       system.debug('printing attachment map::'+attachmentMap);
       for(TransAm_Recruitment_Document__c rd : [SELECT ID, Name, TransAm_Application__c FROM TransAm_Recruitment_Document__c WHERE TransAm_Application__c 
                                                 IN :applicationIds AND Name = 'School Verification Form' ORDER BY Createddate DESC]){                                                      
           if(rd.TransAm_Application__c!=null){
               applicationDocumentMap.put(rd.TransAm_Application__c,rd.Id);    
           }
       }
       for(dsfs__DocuSign_Status__c ds : docuSignStatusList){                
           for(Attachment attach : attachmentMap.get(ds.Id)){
               system.debug('printing attachemnt'+attach);
               Attachment attachmentObj   = new Attachment();
               attachmentObj.Body         = attach.Body;
               attachmentObj.Description  = attach.Description;
               attachmentObj.Name         = attach.Name;
               attachmentObj.ParentId     = applicationDocumentMap.get(applicationDocStatusMap.get(ds.Id));
               attachmentList.add(attachmentObj);
           }
       }
       system.debug('attachment list:::'+attachmentList);
       if(!attachmentList.isEmpty()){
           insert attachmentList;
       }        
   }   
}