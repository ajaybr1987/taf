/******************************************************************************************
* Create By    :     Suresh M
* Create Date  :     02/20/2017
* Description  :     This test class ensures code coverage for TransAm_ApplicationTrigger trigger , TransAm_ApplicationHandler class and TransAm_ApplicationHelper class.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * <Name>                             <Date>           Initial version.
*****************************************************************************************/
@isTest
public class TransAm_ApplicationTriggerTest{
   /************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
        List<Account> lstAccounts = TransAm_Test_DataUtility.createAccounts(1, true);
        List<Contact> lstApplicantContacts = TransAm_Test_DataUtility.createContacts(1, lstAccounts[0].Id, 'Applicant', true);
        List<TransAm_Application__c> lstApplications = new List<TransAm_Application__c>();
        lstApplications = TransAm_Test_DataUtility.createApplications(2, 'Company Driver Only', 'Application Received', lstApplicantContacts[0],false);
        lstApplications.addAll(TransAm_Test_DataUtility.createApplications(1, 'Company Driver Only', 'Application Received', lstApplicantContacts[0],false));
        INSERT lstApplications;
        
        TransAm_Test_DataUtility.createHiringAreasReferences(2,lstApplications[0],true);
        
    }

    /************************************************************************************
    * Method       :    testUpdateApplicationStatus
    * Description  :    Test updateApplicationStatus Functionalities
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testMethod void testUpdateApplicationStatus() {
        List<TransAm_Application__c> lstNewApplications = [SELECT Id, TransAm_ZIP__c, TransAm_Status__c FROM TransAm_Application__c];
        lstNewApplications[0].TransAm_ZIP__c = '12345';
        update lstNewApplications;

        //System.assertEquals(lstNewApplications[0].TransAm_Status__c, 'DQ');
    }

    /************************************************************************************
    * Method       :    testSearchAndLinkAppContacts
    * Description  :    Test searchAndLinkAppContacts Functionalities
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testMethod void testSearchAndLinkAppContacts() {
        List<TransAm_Application__c> lstNewApplications1 = [SELECT Id, TransAm_SSN__c, TransAm_Status__c FROM TransAm_Application__c];
        System.debug('### lstNewApplications1 ###'+lstNewApplications1);
        Contact con = [SELECT ID, TransAm_SSN__c FROM Contact LIMIT 1];
        List<TransAm_Application__c> lstApplications = new List<TransAm_Application__c>();
        lstApplications = TransAm_Test_DataUtility.createApplications(1, 'Company Driver Only', 'Application Received', con,false);
        lstApplications[0].TransAm_SSN__c = '223311441';
        INSERT lstApplications;
                
        //List<Contact> lstApplicantContacts = TransAm_Test_DataUtility.createContacts(1, lstAccounts[0].Id, 'Applicant', false);
        
        lstApplications.clear();
        lstApplications = TransAm_Test_DataUtility.createApplications(1, 'Company Driver Only', 'Application Received', con,false);
        lstApplications[0].TransAm_SSN__c = '999999999';
        lstApplications[0].TransAm_Email__c = 'test1@prod.com';
        lstApplications[0].TransAm_First_Name__c = 'first1';
        lstApplications[0].TransAm_Last_Name__c = 'last1';
        lstApplications[0].TransAm_Phone__c = '1111111111';
        lstApplications[0].TransAm_MobilePhone__c = '1111111111';
        lstApplications[0].TransAm_Applicant__c = null;
        
        insert lstApplications;
        
        lstApplications.clear();
        
        lstApplications = TransAm_Test_DataUtility.createApplications(1, 'Company Driver Only', 'Application Received', con, false);
        insert lstApplications;
     }
     
     public static testMethod void testSearchAndLinkAppContacts2() {
        Account acc = [select id, name from account limit 1];
        
        List<Contact> lstApplicantContacts = TransAm_Test_DataUtility.createContacts(1, acc.Id, 'Applicant', false);
        lstApplicantContacts[0].TransAm_SSN__c = '999999991';
        lstApplicantContacts[0].FirstName = 'first2';
        lstApplicantContacts[0].LastName = 'last2';
        lstApplicantContacts[0].Phone = '1111111112';
        lstApplicantContacts[0].MobilePhone = '1111111112';
        lstApplicantContacts[0].Email = 'test1@prod.com';
        
        insert lstApplicantContacts;
        
        List<TransAm_Application__c> lstApplications = new List<TransAm_Application__c>();
        lstApplications = TransAm_Test_DataUtility.createApplications(1, 'Company Driver Only', 'Application Received', lstApplicantContacts[0], false);     
        lstApplications[0].TransAm_SSN__c = '999999991';
        lstApplications[0].TransAm_Email__c = 'test1@prod.com';
        lstApplications[0].Transam_First_Name__c = 'first2';
        lstApplications[0].TransAm_Last_Name__c = 'last2';
        lstApplications[0].TransAm_Phone__c = '1111111112';
        lstApplications[0].TransAm_MobilePhone__c = '1111111112';
        
        insert lstApplications;
        lstApplications.clear();
        contact con = [select id, name, (select id, name from  Applicants__r) from contact where TransAm_SSN__c = '999999991' limit 1];
        
        system.assertEquals(2, con.Applicants__r.size() );
        
        list<TransAm_Application__c> delAppsList = [select id from TransAm_Application__c where transam_primary_application__c = true and TransAm_SSN__c = '999999991'];
        delete delAppsList;
        
        contact con1 = [select id, name, (select id, name, transam_primary_application__c from  Applicants__r) from contact where TransAm_SSN__c = '999999991' limit 1];
        system.assertEquals(1, con1.Applicants__r.size() );
        system.assertEquals(false, con1.Applicants__r[0].transam_primary_application__c );
        
        lstApplicantContacts.clear();
        
        lstApplicantContacts = TransAm_Test_DataUtility.createContacts(1, acc.Id, 'Applicant', false);
        lstApplicantContacts[0].TransAm_SSN__c = '999999992';
        lstApplicantContacts[0].FirstName = 'first3';
        lstApplicantContacts[0].LastName = 'last3';
        lstApplicantContacts[0].Phone = '1111111113';
        lstApplicantContacts[0].MobilePhone = '1111111113';
        
        insert lstApplicantContacts;
        
        lstApplications = TransAm_Test_DataUtility.createApplications(1, 'Company Driver Only', 'Application Received', lstApplicantContacts[0], false);     
        lstApplications[0].TransAm_SSN__c = '999999991';
        lstApplications[0].TransAm_Email__c = 'test1@prod.com';
        lstApplications[0].Transam_First_Name__c = 'first2';
        lstApplications[0].TransAm_Last_Name__c = 'last2';
        lstApplications[0].TransAm_Phone__c = '1111111112';
        lstApplications[0].TransAm_MobilePhone__c = '1111111112';
        
        insert lstApplications;
    }
    
    public static testMethod void testSearchAndLinkAppContacts3() {
        Account acc = [select id, name from account limit 1];
        
        List<Contact> lstApplicantContacts = TransAm_Test_DataUtility.createContacts(1, acc.Id, 'Applicant', false);
        lstApplicantContacts[0].TransAm_SSN__c = '999999994';
        lstApplicantContacts[0].FirstName = 'first4';
        lstApplicantContacts[0].LastName = 'last4';
        lstApplicantContacts[0].Phone = '1111111114';
        lstApplicantContacts[0].MobilePhone = '1111111114';
        lstApplicantContacts[0].Email = 'test4@prod.com';
        
        insert lstApplicantContacts;
        lstApplicantContacts.clear();
        
        List<Contact> lstApplicantContacts1 = TransAm_Test_DataUtility.createContacts(1, acc.Id, 'Applicant', false);
        lstApplicantContacts1[0].TransAm_SSN__c = '999999995';
        lstApplicantContacts1[0].FirstName = 'first5';
        lstApplicantContacts1[0].LastName = 'last5';
        lstApplicantContacts1[0].Phone = '1111111115';
        lstApplicantContacts1[0].MobilePhone = '1111111115';
        lstApplicantContacts1[0].Email = 'test5@prod.com';
        
        insert lstApplicantContacts1;
        
        list<TransAm_Application__c> lstApplications = TransAm_Test_DataUtility.createApplications(1, 'Company Driver Only', 'Application Received', lstApplicantContacts1[0], false);     
        lstApplications[0].TransAm_SSN__c = '999999994';
        lstApplications[0].TransAm_Email__c = 'test6@prod.com';
        lstApplications[0].Transam_First_Name__c = 'first6';
        lstApplications[0].TransAm_Last_Name__c = 'last6';
        lstApplications[0].TransAm_Phone__c = '1111111116';
        lstApplications[0].TransAm_MobilePhone__c = '1111111116';
        
        insert lstApplications;
    }
    
    public static testMethod void testSearchAndLinkAppContactsformail1() {        
        
        TransAm_Hiring_Areas_Reference__c HAR = [select id, transam_zip__c from TransAm_Hiring_Areas_Reference__c limit 1]; 
        
        Account acc = [select id, name from account limit 1];
        
        List<Contact> lstApplicantContacts = TransAm_Test_DataUtility.createContacts(1, acc.Id, 'Applicant', false);
        lstApplicantContacts[0].TransAm_SSN__c = '999999994';
        lstApplicantContacts[0].FirstName = 'first4';
        lstApplicantContacts[0].LastName = 'last4';
        lstApplicantContacts[0].Phone = '1111111114';
        lstApplicantContacts[0].MobilePhone = '1111111114';
        lstApplicantContacts[0].Email = 'test4@prod.com';       
        
        insert lstApplicantContacts;
                
        list<TransAm_Application__c> lstApplications = TransAm_Test_DataUtility.createApplications(2, 'Company Driver Only', 'Application Received', lstApplicantContacts[0], false);     
        lstApplications[0].TransAm_SSN__c = '999999996';
        lstApplications[0].TransAm_Email__c = 'test4@prod.com';
        lstApplications[0].Transam_First_Name__c = 'first6';
        lstApplications[0].TransAm_Last_Name__c = 'last6';
        lstApplications[0].TransAm_Phone__c = '1111111116';
        lstApplications[0].TransAm_MobilePhone__c = '1111111116';
        lstApplications[0].transam_zip__c = HAR.transam_zip__c;
        
        lstApplications[1].TransAm_SSN__c = '999999996';
        lstApplications[1].TransAm_Email__c = 'test4@prod.com';
        lstApplications[1].Transam_First_Name__c = 'first6';
        lstApplications[1].TransAm_Last_Name__c = 'last6';
        lstApplications[1].TransAm_Phone__c = '1111111116';
        lstApplications[1].TransAm_MobilePhone__c = '1111111116';
        lstApplications[0].transam_zip__c = HAR.transam_zip__c;
        
        insert lstApplications;
        
        list<TransAm_Application__c> lstApplications1 = TransAm_Test_DataUtility.createApplications(1, 'Company Driver Only', 'Application Received', lstApplicantContacts[0], false);     
        lstApplications1[0].TransAm_SSN__c = '999999997';
        lstApplications1[0].TransAm_Email__c = 'test4@prod.com';
        lstApplications1[0].Transam_First_Name__c = 'first7';
        lstApplications1[0].TransAm_Last_Name__c = 'last7';
        lstApplications1[0].TransAm_Phone__c = '1111111117';
        lstApplications1[0].TransAm_MobilePhone__c = '1111111117';
        
        insert lstApplications1;
    }
    
    public static testMethod void testSearchAndLinkAppContactsformail2() {        
        
        TransAm_Hiring_Areas_Reference__c HAR = [select id, transam_zip__c from TransAm_Hiring_Areas_Reference__c limit 1]; 
        
        Account acc = [select id, name from account limit 1];
        
        List<Contact> lstApplicantContacts = TransAm_Test_DataUtility.createContacts(1, acc.Id, 'Applicant', false);
        lstApplicantContacts[0].TransAm_SSN__c = '999999994';
        lstApplicantContacts[0].FirstName = 'first4';
        lstApplicantContacts[0].LastName = 'last4';
        lstApplicantContacts[0].Phone = '1111111114';
        lstApplicantContacts[0].MobilePhone = '1111111114';
        lstApplicantContacts[0].Email = 'test4@prod.com';
        
        insert lstApplicantContacts;
                
        list<TransAm_Application__c> lstApplications = TransAm_Test_DataUtility.createApplications(2, 'Company Driver Only', 'Application Received', lstApplicantContacts[0], false);     
        lstApplications[0].TransAm_SSN__c = '999999996';
        lstApplications[0].TransAm_Email__c = 'test4@prod.com';
        lstApplications[0].Transam_First_Name__c = 'first6';
        lstApplications[0].TransAm_Last_Name__c = 'last6';
        lstApplications[0].TransAm_Phone__c = '1111111116';
        lstApplications[0].TransAm_MobilePhone__c = '1111111116';
        lstApplications[0].transam_zip__c = HAR.transam_zip__c;
        
        insert lstApplications;
        
        TransAm_Application__c app = [select id from TransAm_Application__c where TransAm_SSN__c = '999999996' limit 1];
        delete app;
        
        list<TransAm_Application__c> lstApplications1 = TransAm_Test_DataUtility.createApplications(2, 'Company Driver Only', 'Application Received', lstApplicantContacts[0], false);     
        lstApplications1[0].TransAm_SSN__c = '999999996';
        lstApplications1[0].TransAm_Email__c = 'test4@prod.com';
        lstApplications1[0].Transam_First_Name__c = 'first6';
        lstApplications1[0].TransAm_Last_Name__c = 'last6';
        lstApplications1[0].TransAm_Phone__c = '1111111116';
        lstApplications1[0].TransAm_MobilePhone__c = '1111111116';
        lstApplications1[0].transam_zip__c = HAR.transam_zip__c;
        
        insert lstApplications1;
    }
    
    public static testMethod void testSearchAndLinkAppContactsformail3() {        
        
        TransAm_Hiring_Areas_Reference__c HAR = [select id, transam_zip__c from TransAm_Hiring_Areas_Reference__c limit 1]; 
        
        Account acc = [select id, name from account limit 1];
        
        List<Contact> lstApplicantContacts = TransAm_Test_DataUtility.createContacts(1, acc.Id, 'Applicant', false);
        lstApplicantContacts[0].TransAm_SSN__c = '999999994';
        lstApplicantContacts[0].FirstName = 'first4';
        lstApplicantContacts[0].LastName = 'last4';
        lstApplicantContacts[0].Phone = '1111111114';
        lstApplicantContacts[0].MobilePhone = '1111111114';
        lstApplicantContacts[0].Email = 'test4@prod.com';
        
        insert lstApplicantContacts;
        
        List<Contact> lstApplicantContacts1 = TransAm_Test_DataUtility.createContacts(1, acc.Id, 'Applicant', false);
        lstApplicantContacts1[0].TransAm_SSN__c = '999999995';
        lstApplicantContacts1[0].FirstName = 'first5';
        lstApplicantContacts1[0].LastName = 'last5';
        lstApplicantContacts1[0].Phone = '1111111115';
        lstApplicantContacts1[0].MobilePhone = '1111111115';
        lstApplicantContacts1[0].Email = 'test5@prod.com';
        
        insert lstApplicantContacts1;
                
        list<TransAm_Application__c> lstApplications = TransAm_Test_DataUtility.createApplications(2, 'Company Driver Only', 'Application Received', lstApplicantContacts1[0], false);     
        lstApplications[0].TransAm_SSN__c = '999999996';
        lstApplications[0].TransAm_Email__c = 'test4@prod.com';
        lstApplications[0].Transam_First_Name__c = 'first6';
        lstApplications[0].TransAm_Last_Name__c = 'last6';
        lstApplications[0].TransAm_Phone__c = '1111111116';
        lstApplications[0].TransAm_MobilePhone__c = '1111111116';
        lstApplications[0].transam_zip__c = HAR.transam_zip__c;
        
        insert lstApplications;
    }
    
    public static testMethod void testSearchAndLinkAppContactsforphone1() {        
        
        TransAm_Hiring_Areas_Reference__c HAR = [select id, transam_zip__c from TransAm_Hiring_Areas_Reference__c limit 1]; 
        
        Account acc = [select id, name from account limit 1];
        
        List<Contact> lstApplicantContacts = TransAm_Test_DataUtility.createContacts(1, acc.Id, 'Applicant', false);
        lstApplicantContacts[0].TransAm_SSN__c = '999999994';
        lstApplicantContacts[0].FirstName = 'first4';
        lstApplicantContacts[0].LastName = 'last4';
        lstApplicantContacts[0].Phone = '1111111114';
        lstApplicantContacts[0].MobilePhone = '1111111114';
        lstApplicantContacts[0].Email = 'test4@prod.com';       
        
        insert lstApplicantContacts;
                
        list<TransAm_Application__c> lstApplications = TransAm_Test_DataUtility.createApplications(2, 'Company Driver Only', 'Application Received', lstApplicantContacts[0], false);     
        lstApplications[0].TransAm_SSN__c = '999999996';
        lstApplications[0].TransAm_Email__c = 'test5@prod.com';
        lstApplications[0].Transam_First_Name__c = 'first4';
        lstApplications[0].TransAm_Last_Name__c = 'last4';
        lstApplications[0].TransAm_Phone__c = '1111111114';
        lstApplications[0].TransAm_MobilePhone__c = '1111111116';
        lstApplications[0].transam_zip__c = HAR.transam_zip__c;
        
        lstApplications[0].TransAm_SSN__c = '999999996';
        lstApplications[0].TransAm_Email__c = 'test5@prod.com';
        lstApplications[0].Transam_First_Name__c = 'first4';
        lstApplications[0].TransAm_Last_Name__c = 'last4';
        lstApplications[0].TransAm_Phone__c = '1111111114';
        lstApplications[0].TransAm_MobilePhone__c = '1111111116';
        lstApplications[0].transam_zip__c = HAR.transam_zip__c;
        
        insert lstApplications;
        
        list<TransAm_Application__c> lstApplications1 = TransAm_Test_DataUtility.createApplications(1, 'Company Driver Only', 'Application Received', lstApplicantContacts[0], false);     
        lstApplications1[0].TransAm_SSN__c = '999999997';
        lstApplications1[0].TransAm_Email__c = 'test4@prod.com';
        lstApplications1[0].Transam_First_Name__c = 'first4';
        lstApplications1[0].TransAm_Last_Name__c = 'last4';
        lstApplications1[0].TransAm_Phone__c = '1111111116';
        lstApplications1[0].TransAm_MobilePhone__c = '1111111117';
        
        insert lstApplications1;
    }
    
    public static testMethod void testSearchAndLinkAppContactsforcell1() {        
        
        TransAm_Hiring_Areas_Reference__c HAR = [select id, transam_zip__c from TransAm_Hiring_Areas_Reference__c limit 1]; 
        
        Account acc = [select id, name from account limit 1];
        
        List<Contact> lstApplicantContacts = TransAm_Test_DataUtility.createContacts(1, acc.Id, 'Applicant', false);
        lstApplicantContacts[0].TransAm_SSN__c = '999999994';
        lstApplicantContacts[0].FirstName = 'first4';
        lstApplicantContacts[0].LastName = 'last4';
        lstApplicantContacts[0].Phone = '1111111116';
        lstApplicantContacts[0].MobilePhone = '1111111114';
        lstApplicantContacts[0].Email = 'test4@prod.com';       
        
        insert lstApplicantContacts;
                
        list<TransAm_Application__c> lstApplications = TransAm_Test_DataUtility.createApplications(2, 'Company Driver Only', 'Application Received', lstApplicantContacts[0], false);     
        lstApplications[0].TransAm_SSN__c = '999999996';
        lstApplications[0].TransAm_Email__c = 'test5@prod.com';
        lstApplications[0].Transam_First_Name__c = 'first4';
        lstApplications[0].TransAm_Last_Name__c = 'last4';
        lstApplications[0].TransAm_Phone__c = '1111111115';
        lstApplications[0].TransAm_MobilePhone__c = '1111111114';
        lstApplications[0].transam_zip__c = HAR.transam_zip__c;
        
        lstApplications[1].TransAm_SSN__c = '999999996';
        lstApplications[1].TransAm_Email__c = 'test5@prod.com';
        lstApplications[1].Transam_First_Name__c = 'first4';
        lstApplications[1].TransAm_Last_Name__c = 'last4';
        lstApplications[1].TransAm_Phone__c = '1111111115';
        lstApplications[1].TransAm_MobilePhone__c = '1111111114';
        lstApplications[1].transam_zip__c = HAR.transam_zip__c;
        
        insert lstApplications;         
    }
       
       public static testMethod void testSearchAndLinkAppContactsforunmatch() {        
        
        TransAm_Hiring_Areas_Reference__c HAR = [select id, transam_zip__c from TransAm_Hiring_Areas_Reference__c limit 1]; 
        
        Account acc = [select id, name from account limit 1];
        
        List<Contact> lstApplicantContacts = TransAm_Test_DataUtility.createContacts(1, acc.Id, 'Applicant', false);
        lstApplicantContacts[0].TransAm_SSN__c = '999999994';
        lstApplicantContacts[0].FirstName = 'first4';
        lstApplicantContacts[0].LastName = 'last4';
        lstApplicantContacts[0].Phone = '1111111114';
        lstApplicantContacts[0].MobilePhone = '1111111114';
        lstApplicantContacts[0].Email = 'test4@prod.com';       
        
        insert lstApplicantContacts;
        
         list<TransAm_Application__c> lstApplications = TransAm_Test_DataUtility.createApplications(2, 'Company Driver Only', 'Application Received', lstApplicantContacts[0], false);     
        lstApplications[0].TransAm_SSN__c = '999999996';
        lstApplications[0].TransAm_Email__c = 'test5@prod.com';
        lstApplications[0].Transam_First_Name__c = 'first5';
        lstApplications[0].TransAm_Last_Name__c = 'last5';
        lstApplications[0].TransAm_Phone__c = '1111111115';
        lstApplications[0].TransAm_MobilePhone__c = '1111111115';
        lstApplications[0].transam_zip__c = HAR.transam_zip__c;
        
        insert lstApplications;
        
        }

       
    
    private static testmethod void testDQStatus1(){
        List<TransAm_Application__c> lstNewApplications = [SELECT Id, TransAm_SSN__c, TransAm_Status__c FROM TransAm_Application__c];
        lstNewApplications[0].TransAm_Date_Of_Birth__c = Date.today().addYears(-15);
        update lstNewApplications;
        List<TransAm_Application__c> updatedApp = [Select Id, TransAm_Status__c, TransAm_Status_Reason__c FROM TransAm_Application__c WHERE ID =:lstNewApplications[0].Id];
        //System.assertEquals('Under 21',updatedApp[0].TransAm_Status_Reason__c);
    }
    private static testmethod void testDQStatus2(){
        List<TransAm_Application__c> lstNewApplications = [SELECT Id, TransAm_SSN__c, TransAm_Status__c,TransAm_EBE_Entry_ID__c FROM TransAm_Application__c LIMIT 1];        
        List<TransAm_Violation__c> violations = TransAm_Test_DataUtility.createViolations(1,lstNewApplications[0],false);
        violations[0].TransAm_Charges__c = 'DRIVING UNDER THE INFLUENCE';
        violations[0].TransAm_Conviction_Date__c = Date.today().addmonths(-4);
        INSERT violations;
        List<TransAm_Application__c> updatedApp = [Select Id, TransAm_Status__c, TransAm_Status_Reason__c FROM TransAm_Application__c WHERE ID =:lstNewApplications[0].Id];
        //System.assertEquals('One or More DUIs in 2 Years',updatedApp[0].TransAm_Status_Reason__c);
                
    }
    private static testmethod void testDQStatus3(){
        List<TransAm_Application__c> lstNewApplications = [SELECT Id, TransAm_SSN__c, TransAm_Status__c,TransAm_EBE_Entry_ID__c FROM TransAm_Application__c];
        List<TransAm_Violation__c> violations = TransAm_Test_DataUtility.createViolations(1,lstNewApplications[0],false);
        violations[0].TransAm_Charges__c = 'RECKLESS DRIVING';
        violations[0].TransAm_Conviction_Date__c = Date.today().addmonths(-4);
        INSERT violations;
        List<TransAm_Application__c> updatedApp = [Select Id, TransAm_Status__c, TransAm_Status_Reason__c FROM TransAm_Application__c WHERE ID =:lstNewApplications[0].Id];
        //System.assertEquals('Reckless Last 5 Years',updatedApp[0].TransAm_Status_Reason__c);
        
    }
    private static testmethod void testDQStatus4(){
        List<TransAm_Application__c> lstNewApplications = [SELECT Id, TransAm_SSN__c, TransAm_Status__c,TransAm_EBE_Entry_ID__c FROM TransAm_Application__c];
        List<TransAm_Accident__c> accidents = TransAm_Test_DataUtility.createAccidents(3,lstNewApplications[0],false);
        accidents[0].TransAm_Date__c = Date.Today().addDays(-100);
        accidents[1].TransAm_Date__c = Date.Today().addDays(-110);
        accidents[2].TransAm_Date__c = Date.Today().addDays(-120);
        INSERT accidents;
        List<TransAm_Application__c> updatedApp = [Select Id, TransAm_Status__c, TransAm_Status_Reason__c FROM TransAm_Application__c WHERE ID =:lstNewApplications[0].Id];
        //System.assertEquals('More than 2 Preventable Accidents in the last 3 years (doesn\'t count for students)',updatedApp[0].TransAm_Status_Reason__c);
        
    }
    private static testmethod void testJobHistoryValidation1(){
        try{
            List<TransAm_Application__c> lstNewApplications = [SELECT Id, TransAm_SSN__c, TransAm_Status__c,TransAm_EBE_Entry_ID__c FROM TransAm_Application__c];
            List<TransAm_Employer__c> employer = TransAm_Test_DataUtility.createEmployers(1,true);
            List<TransAm_Job_History__c> histories = TransAm_Test_DataUtility.createJobHistory(2,lstNewApplications[0],employer[0],false);
            histories[0].TransAm_Employed_From__c = Date.today().addMonths(-12);
            histories[0].TransAm_Employed_To__c = Date.today().addMonths(-2);
            histories[1].TransAm_Employed_From__c = Date.today().addMonths(-24);
            histories[1].TransAm_Employed_To__c = Date.today().addMonths(-14);
            INSERT histories;
            lstNewApplications[0].TransAm_Status__c = 'Manager Review';
            UPDATE lstNewApplications;
        }
        catch(Exception e)
        {
            Boolean expectedExceptionThrown =  e.getMessage().contains('There is a gap in the employment history, please review the information and correct any errors.') ? true : false;
            //System.AssertEquals(expectedExceptionThrown, true);
        } 
        
         
    }
     private static testmethod void testJobHistoryValidation2(){
         try{
             List<TransAm_Application__c> lstNewApplications = [SELECT Id, TransAm_SSN__c, TransAm_Status__c,TransAm_EBE_Entry_ID__c FROM TransAm_Application__c];       
             lstNewApplications[0].TransAm_Status__c = 'Manager Review';
             UPDATE lstNewApplications;
             //System.assert(lstNewApplications[0].TransAm_Status__c != 'Manager Review');
         }
         catch(Exception e)
         {
             Boolean expectedExceptionThrown =  e.getMessage().contains('There is a gap in the employment history, please review the information and correct any errors.') ? true : false;
             //System.AssertEquals(expectedExceptionThrown, true);
         } 
    }
    private static testmethod void childAppCreationIndicationException(){
        try{
        Test.startTest();       
            TransAm_ApplicationHelper.childAppCreationIndication(null);
        Test.stopTest();
        }catch(Exception e){
            
        }   
    }   
     private static testmethod void testTerminationDate(){
        List<TransAm_Application__c> lstNewApplications = [SELECT Id, TransAm_SSN__c, TransAm_Status__c,TransAm_EBE_Entry_ID__c,TransAm_Emp_Status__c FROM TransAm_Application__c];
        lstNewApplications[0].TransAm_Emp_Status__c ='Term – No Rehire';
        UPDATE lstNewApplications;  
    }
    private static testmethod void testDriverCode(){
        List<TransAm_Application__c> lstNewApplications = [SELECT Id, TransAm_SSN__c, TransAm_Status__c,TransAm_EBE_Entry_ID__c,TransAm_DriverID__c FROM TransAm_Application__c];
        lstNewApplications[0].TransAm_DriverID__c ='345345';
        UPDATE lstNewApplications;  
    }
    private static testmethod void updateSSNFields(){
        List<TransAm_Application__c> lstNewApplications = [SELECT Id, TransAm_SSN__c, TransAm_Status__c,TransAm_EBE_Entry_ID__c,TransAm_First_Name__c FROM TransAm_Application__c];
        lstNewApplications[0].TransAm_First_Name__c ='REATest';
        UPDATE lstNewApplications;  
    }
    private static testmethod void updateUniqueFields(){
        List<TransAm_Application__c> lstNewApplications = [SELECT Id, TransAm_SSN__c, TransAm_Status__c,TransAm_EBE_Entry_ID__c,TransAm_First_Name__c FROM TransAm_Application__c];
        lstNewApplications[0].TransAm_First_Name__c ='REATest';
        lstNewApplications[0].TransAm_Last_Name__c ='trte45';
        lstNewApplications[0].TransAm_Email__c ='trte45@wert.com';
        lstNewApplications[0].TransAm_Phone__c ='7878787878';
        lstNewApplications[0].TransAm_SSN__c ='3434343434';
        lstNewApplications[0].TransAm_MobilePhone__c ='1212121212';
        UPDATE lstNewApplications;  
    }
    private static testmethod void updateZipFields(){
        TransAm_Hiring_Areas_Reference__c objH = new TransAm_Hiring_Areas_Reference__c(TransAm_ZIP__c = '00123');
        INSERT objH;
        List<TransAm_Application__c> lstNewApplications = [SELECT Id, TransAm_SSN__c, TransAm_Status__c,TransAm_EBE_Entry_ID__c,TransAm_First_Name__c FROM TransAm_Application__c];
        lstNewApplications[0].TransAm_Zip__c ='00123';
        lstNewApplications[0].TransAm_Emp_Status__c ='Term – No Rehire';        
        UPDATE lstNewApplications;  
    }
    
}