/******************************************************************************************
* Created By   :     Pankaj SIngh
* Create Date  :     03/19/2017
* Description  :     Helper class for TransAm_DocuSignStatusTrigger.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * <Name>                             <Date>           Initial version.
*****************************************************************************************/

public class TransAm_DocuSignStatusHelper{

  /*******************************************************************************************
   * Method        :   updateApplicationStatus
   * Description   :   Update application status based on the docusign status envelope status.
   * Parameter     :   list<dsfs__DocuSign_Status__c>
   * Return Type   :   void
   ******************************************************************************************/
    public static void updateApplicationStatus(list<dsfs__DocuSign_Status__c> docuSignStatusList){
        
        set<Id> applicationIds = new set<Id>();   
        set<Id> docuSignStatusIds = new set<Id>();               
        list<TransAm_Application__c> applicationsToBeUpdated = new list<TransAm_Application__c>();
        list<TransAm_Error_Log__c> errorLogObjList = new list<TransAm_Error_Log__c>();
        map<Id,TransAm_Application__c> applicationMap = new map<Id,TransAm_Application__c>();
        Database.SaveResult[] saveResult;
        try{
            for(dsfs__DocuSign_Status__c ds : docuSignStatusList){
                docuSignStatusIds.add(ds.Id);
                applicationIds.add(ds.TransAm_Application__c);
            }                                        
            for(TransAm_Application__c app : [SELECT ID,TransAm_Status__c,TransAm_Sub_status__c FROM TransAm_Application__c WHERE ID IN : applicationIds]){
                applicationMap.put(app.Id,app);                
            }            
            for(dsfs__DocuSign_Status__c ds : docuSignStatusList){
                if(applicationMap.containsKey(ds.TransAm_Application__c)){
                    if(ds.dsfs__Envelope_Status__c == 'Sent'){               
                        applicationMap.get(ds.TransAm_Application__c).TransAm_Status__c = 'Waiting on Release Form';
                    }
                    if(ds.dsfs__Envelope_Status__c == 'Completed'){
                        system.debug('inside completed:::');
                        applicationMap.get(ds.TransAm_Application__c).TransAm_Status__c = 'Release Form Received';
                    }
                    applicationsToBeUpdated.add(applicationMap.get(ds.TransAm_Application__c));
                }
            }
            if(!docuSignStatusIds.isEmpty()){
                //TransAm_MigrateDocuSignDocuments.transferDocumentsToRD(docuSignStatusIds);
            }
            if(!applicationsToBeUpdated.isEmpty()){
                saveResult = Database.update(applicationsToBeUpdated,false);
            }
         }catch(Exception excep){
             for(Database.SaveResult sr : saveResult) {
                 for(Database.Error err : sr.getErrors()){                           
                     TransAm_Error_Log__c TransAmErrorLogObj = new TransAm_Error_Log__c();                            
                     TransAmErrorLogObj.TransAm_Class_Name__c = 'TransAm_DocuSignStatusHelper';
                     TransAmErrorLogObj.TransAm_Exception_Message__c = err.getMessage();
                     TransAmErrorLogObj.TransAm_Method_Name__c = 'updateApplicationStatus';
                     TransAmErrorLogObj.TransAm_Module_Name__c = 'Release Form Collection';                            
                     errorLogObjList.add(TransAmErrorLogObj);
                 }
             }
             TransAm_Error_Log__c TransAmErrorLogObj = new TransAm_Error_Log__c();                            
             TransAmErrorLogObj.TransAm_Class_Name__c = 'TransAm_DocuSignStatusHelper';
             TransAmErrorLogObj.TransAm_Exception_Message__c = excep.getMessage();
             TransAmErrorLogObj.TransAm_Method_Name__c = 'updateApplicationStatus';
             TransAmErrorLogObj.TransAm_Module_Name__c = 'Release Form Collection';
             errorLogObjList.add(TransAmErrorLogObj); 
             if(!errorLogObjList.isEmpty()){
                 insert errorLogObjList;
             }
         }                
    }
}