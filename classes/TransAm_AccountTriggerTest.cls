/******************************************************************************************
* Create By    :     Pardeep Rao
* Create Date  :     03/06/2017
* Description  :     This test class ensures code coverage for TransAm_AccountTrigger trigger , 
*         TransAm_AccountHandler class and TransAm_AccountHelper class.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * <Name>                             <Date>           Initial version.
*****************************************************************************************/
@isTest(seeAllData=false)
public class TransAm_AccountTriggerTest{ 

    public static testmethod void afterInsertSchoolTest(){
    
        Account acc = new Account();
        acc.Name='Test Driving School';
        acc.TransAm_Driving_School_Code__c='1234TESTSCHOOL';
        
        Test.startTest();
            insert acc;
        Test.stopTest();
    }
    
    
    /************************************************************************************
    * Method       :    testAccountHandlerException
    * Description  :    Test Method to cover exception block
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testAccountHandlerException() {
    
        Test.startTest();
            TransAm_AccountHelper.insertPrivateRecruiterComment(new List<Account>());
        Test.stopTest();
      }
   

}