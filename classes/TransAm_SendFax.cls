global class TransAm_SendFax {
    
    webservice static void sendFaxToEmployer(Id jobHistoryId, String faxNumber){
        ID signedDocId, verifyDocId;
        TransAm_Job_History__c objApp = [SELECT TransAm_Application__c FROM TransAm_Job_History__c WHERE ID=:jobHistoryId LIMIT 1];
        Id appId = objApp.TransAm_Application__c;
        List<TransAm_Recruitment_Document__c> recrList = [Select Id,TransAm_Type__c FROM TransAm_Recruitment_Document__c WHERE TransAm_Application__c =: appId AND (TransAm_Type__c = 'Background Check Release Form' OR TransAm_Type__c ='Previous Employment Verification') ];
        for(TransAm_Recruitment_Document__c objR : recrList){
            if(objR.TransAm_Type__c == 'Background Check Release Form')
                signedDocId =objR.Id;
            else if(objR.TransAm_Type__c == 'Previous Employment Verification')
                verifyDocId = objR.Id;
        }
        if(signedDocId != null){
            Attachment att;
        	List<Attachment> attList = [Select Id FROM Attachment Where PArentId =: signedDocId LIMIT 1];
        	if(!attList.isEmpty())
                att = attList[0];
            System.debug('###'+att);
        
            efaxapp__Sent_Fax__c fax = new efaxapp__Sent_Fax__c(efaxapp__Fax_Number__c = faxNumber,efaxapp__Subject__c = 'Transam Fax',efaxapp__Send_Date__c = Datetime.now(),efaxapp__Attachment_ID__c = att.Id,efaxapp__Status__c = 'Sending',
                efaxapp__Sent_With_Outbound_Message__c = True, 
                efaxapp__Org_Fax_Number__c = Label.TransAm_Org_Fax_Number, 
                efaxapp__Barcode_Position_Top__c = '11', 
                efaxapp__Barcode_Position_Left__c = '404',
                efaxapp__Barcode_Size_Width__c = '72',
                efaxapp__Preview_Image_URL__c = '/servlet/servlet.FileDownload?file='+att.Id,
                efaxapp__Fax_URL__c = '/servlet/servlet.FileDownload?file='+att.Id, 
                TransAm_Recruitment_Document__c = verifyDocId
            );
            System.debug('efaxapp__Sent_Fax__c -->'+fax);
            insert fax;
            }
    }
    
    webservice static void sendFaxToSchoolContact(Id appId){
        ID signedDocId, verifyDocId;
        List<TransAm_Recruitment_Document__c> recrList = [Select Id,TransAm_Type__c FROM TransAm_Recruitment_Document__c WHERE TransAm_Application__c =: appId AND (TransAm_Type__c = 'School Verification Release Form' OR TransAm_Type__c = 'School Verification Form')];
        System.debug('%%%'+recrList);
        for(TransAm_Recruitment_Document__c objR : recrList){
            if(objR.TransAm_Type__c == 'School Verification Release Form')
                signedDocId =objR.Id;
            else if(objR.TransAm_Type__c == 'School Verification Form')
                verifyDocId = objR.Id;
            System.debug('##RA'+signedDocId +'-->'+verifyDocId);
        }
        System.debug('##RA2 -->'+signedDocId +'-->'+verifyDocId);
        if(signedDocId != null){
            Attachment att;
            List<Attachment> attList = [Select Id FROM Attachment Where PArentId =: signedDocId LIMIT 1];
            if(!attList.isEmpty())
                att = attList[0];
            TransAm_Application__c objApp = [SELECT Id, TransAm_Applicant__r.Account.Fax FROM TransAm_Application__c WHERE ID=: appId LIMIT 1];
            //if(objApp.TransAm_Applicant__r.Account.Fax !=null){
                String faxNumber = objApp.TransAm_Applicant__r.Account.Fax;
                
                efaxapp__Sent_Fax__c fax = new efaxapp__Sent_Fax__c(efaxapp__Fax_Number__c = faxNumber,efaxapp__Subject__c = 'Transam Fax',efaxapp__Send_Date__c = Datetime.now(),efaxapp__Attachment_ID__c = att.Id,efaxapp__Status__c = 'Sending',
                                                                    efaxapp__Sent_With_Outbound_Message__c = True, 
                                                                    efaxapp__Org_Fax_Number__c = Label.TransAm_Org_Fax_Number, 
                                                                    efaxapp__Barcode_Position_Top__c = '11', 
                                                                    efaxapp__Barcode_Position_Left__c = '404',
                                                                    efaxapp__Barcode_Size_Width__c = '72',
                                                                    efaxapp__Preview_Image_URL__c = '/servlet/servlet.FileDownload?file='+att.Id,
                                                                    efaxapp__Fax_URL__c = '/servlet/servlet.FileDownload?file='+att.Id,
                                                                    TransAm_Recruitment_Document__c = verifyDocId
                                                                   );
                System.debug('efaxapp__Sent_Fax__c -->'+fax);
                insert fax;
            //}
            
        }
    }
}