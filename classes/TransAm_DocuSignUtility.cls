/* Class Name   : TransAm_DocuSignUtility 
 * Description  : TUtility class for sending documents to contacts associated with leads using docusign
 * Created By   : Pankaj Singh
 * Created On   : 25-Jun-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj SIngh                22-Jun-2016             Initial version.
 *
 *****************************************************************************************/
public with sharing class TransAm_DocuSignUtility {
    
    public string applicantId {get;set;}
    private string receipientList = '';
    private decimal routingNo = 0;
    
    public TransAm_DocuSignUtility(){
        applicantId = ApexPages.currentPage().getParameters().get('Id');
    }
    public PageReference sendToDocuSign(){
    
        TransAm_Application__c appliationObj;
        appliationObj = [Select id,Owner.Name,TransAm_Driver_Type__c,TransAm_State__c,TransAm_Orientation_Location__c,TransAm_First_Name__c,Owner.Email,TransAm_Last_Name__c,TransAm_Email__c from TransAm_Application__c WHERE ID =: applicantId];        
        string templateId = '';
        string RC = '';
        string RSL='';
        string RSRO='';
        string RROS='';
        string CCRM='';
        string CCTM='';
        string CCNM='';
        string CRCL=''; 
        string CRL='';
        string OCO='';
        string DST='';
        string LA='';
        string CEM='';
        string CES='';
        string STB='';
        string SSB='';
        string SES='';
        string SEM='';string SRS='';string SCS ='';string RES=''; 
        //DST='ab8269a7-474a-458d-8797-118ca95d8bc1';
        if(appliationObj.TransAm_Driver_Type__c == 'Owner'){ 
            templateId='16b73fe2-07b4-437d-ba97-2c7795c7ecd8';
        }else if(appliationObj.TransAm_Driver_Type__c == 'Company' && appliationObj.TransAm_Orientation_Location__c == 'Olathe, KS' && appliationObj.TransAm_State__c == 'KS'){
            templateId='a19f76bd-b9f9-466a-b06d-9046e259bcad';
        }else if(appliationObj.TransAm_Driver_Type__c == 'Company' && appliationObj.TransAm_Orientation_Location__c == 'Rockwall, TX'){
            templateId='2d85f7e4-d06c-42ab-9b2a-3eeff8d63bb6';
        }else if(appliationObj.TransAm_Driver_Type__c == 'Company' && appliationObj.TransAm_Orientation_Location__c == 'Olathe, KS' && appliationObj.TransAm_State__c != 'KS'){
            templateId='1ccf13c4-b427-496e-b8ef-4b985f4f69a3';
        }  
        //Adding Notes & Attachments 
        LA='0'; 
        //Custom Recipient List 
        Id userId = UserInfo.getUserId();
        User activeUser = new User();
        if(userId!=null){
            activeUser = [Select Id,Email,FirstName,LastName From User where Id = : userId limit 1];
        }
        receipientList = receipientList +'Email~'+appliationObj.TransAm_Email__c+';FirstName~'+appliationObj.TransAm_First_Name__c+';LastName~'+appliationObj.TransAm_Last_Name__c+';Role~Signer1;RoutingOrder~1;';
        receipientList = receipientList +','+Label.TransAm_DocusignUtility_Signer2+'Role~Signer2;RoutingOrder~2';
        
        CRL= receipientList + 'LoadDefaultContacts~0';
        
        // Show Email Subject (default in config) 
        SES = '1'; //Ex: '1' 
        // Show Email Message (default in config) 
        SEM = '1'; //Ex: '1' 
        // Show Tag Button (default in config) 
        STB = '1'; //Ex: '1' 
        // Show Chatter (default in config) 
        SCS = '0'; //Ex: '1' 
        OCO = ''; 
        PageReference pageRef = null;
        pageRef = new PageReference('/apex/dsfs__DocuSign_CreateEnvelope?DSEID=0&SourceID='+applicantId+'&RC='+RC+'&RSL='+RSL+'&RSRO='+RSRO+'&RROS='+RROS+'&CCRM='+CCRM+'&CCTM='+CCTM+'&CRCL='+CRCL+'&CRL='+CRL+'&OCO='+OCO+'&DST='+templateId+'&CCNM='+CCNM+'&LA='+LA+'&CEM='+CEM+'&CES='+CES+'&SRS='+SRS+'&STB='+STB+'&SSB='+SSB+'&SES='+SES+'&SEM='+SEM+'&SRS='+SRS+'&SCS='+SCS+'&RES='+RES);
        pageRef.setRedirect(true);    
        return pageRef;
   }
}