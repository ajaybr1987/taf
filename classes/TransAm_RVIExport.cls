global class TransAm_RVIExport{
    webservice static void generateXML(Id appId){
		
        TransAm_Application__c currentApp = [SELECT Id,TransAm_First_Name__c,TransAm_Last_Name__c,TransAm_Status__c,TransAm_Orientation_Date__c,
                                             Name,TransAm_DriverID__c,TransAm_Driver_Type__c,TransAm_RVI_Export__c,TransAm_RVI_Export_Date__c,TransAm_SSN__c
                                             FROM TransAm_Application__c WHERE ID=:appId LIMIT 1];
        List<TransAm_Recruitment_Document__c> recruitmentDocList = [SELECT Id, TransAm_Type__c, TransAm_SSN__c, TransAm_Application__c,TransAm_Is_Exported__c FROM TransAm_Recruitment_Document__c WHERE TransAm_Application__c =:appId];
    	List<Attachment> attachList = [SELECT Id, Name, ParentId FROM Attachment where ParentId IN: recruitmentDocList];
        List<RVI_XML_Settings__mdt> metasetting = [SELECT Id, TransAm_All_Drivers_Hired_System__c, TransAm_Company_Hired_System__c,TransAm_Document_Name__c,
                                                  TransAm_Owner_Hired_System__c,TransAm_RVI_Scan_Code__c,TransAm_Task_21_RejectedDQed__c,TransAm_Task_25_No_Contact__c,TransAm_Task_X_In_Progress__c
                                                  FROM RVI_XML_Settings__mdt];
        List<formatXML> xmlNodes = new List<formatXML>();
        List<Attachment> updateAttachList = new List<Attachment>();
        for(Attachment objAttachment : attachList){
            for(TransAm_Recruitment_Document__c objR : recruitmentDocList){
                if(objR.Id == objAttachment.ParentId && objR.TransAm_Type__c != 'System Document'){
                    formatXML objXML = new formatXML();
                    objXML.recruitDocType = objR.TransAm_Type__c;
                    objXML.docName = objAttachment.Name;
                    objXML.docId = objR.Id;
                    objXML.typeofDoc ='O';
                    xmlNodes.add(objXML);
                    objR.TransAm_Is_Exported__c = true;
                    		                  
                }
            }
        }
        System.debug('###RA'+attachList);
        if(currentApp.TransAm_Status__c == 'Application Complete'){
            for(RVI_XML_Settings__mdt objSetting : metasetting){
                for(formatXML objXML : xmlNodes){
                    if(String.isNotBlank(objSetting.TransAm_All_Drivers_Hired_System__c) && objXML.recruitDocType == objSetting.TransAm_Document_Name__c){
                        objXML.scanCode = objSetting.TransAm_RVI_Scan_Code__c;
                        objXML.folderLocation = objSetting.TransAm_All_Drivers_Hired_System__c;   
                    }
                    if(currentApp.TransAm_Driver_Type__c == 'Company'){
                        if(currentApp.TransAm_Orientation_Date__c!=null)
                            objXML.hireDate = (datetime.newInstance(currentApp.TransAm_Orientation_Date__c.year(), currentApp.TransAm_Orientation_Date__c.month(),currentApp.TransAm_Orientation_Date__c.day())).format('MMddyy');
                        if(String.isNotBlank(objSetting.TransAm_Company_Hired_System__c) && objXML.recruitDocType == objSetting.TransAm_Document_Name__c){
                            objXML.driverType = objSetting.TransAm_Company_Hired_System__c;
                            objXML.scanCode = objSetting.TransAm_RVI_Scan_Code__c;
                            //if(currentApp.TransAm_Orientation_Date__c!=null)
                            //objXML.hireDate = (datetime.newInstance(currentApp.TransAm_Orientation_Date__c.year(), currentApp.TransAm_Orientation_Date__c.month(),currentApp.TransAm_Orientation_Date__c.day())).format('MM/dd/yy');
                        }
                    }
                    else if(currentApp.TransAm_Driver_Type__c == 'Owner'){
                        if(currentApp.TransAm_Orientation_Date__c != null){
                                Date hireUpdatedDate=currentApp.TransAm_Orientation_Date__c.addDays(2);
                            	objXML.hireDate = (datetime.newInstance(hireUpdatedDate.year(), hireUpdatedDate.month(),hireUpdatedDate.day())).format('MMddyy') ;
                        }
                    	if(String.isNotBlank(objSetting.TransAm_Owner_Hired_System__c) && objXML.recruitDocType == objSetting.TransAm_Document_Name__c){
                            objXML.driverType = objSetting.TransAm_Owner_Hired_System__c;
                            objXML.scanCode = objSetting.TransAm_RVI_Scan_Code__c;
                            /*if(currentApp.TransAm_Orientation_Date__c != null){
                                Date hireUpdatedDate=currentApp.TransAm_Orientation_Date__c.addDays(2);
                            	objXML.hireDate = (datetime.newInstance(hireUpdatedDate.year(), hireUpdatedDate.month(),hireUpdatedDate.day())).format('MM/dd/yy') ;
                            }*/
                    	}
                    }
                }
                
            }
        }
        else if(currentApp.TransAm_Status__c == 'Application Received'|| currentApp.TransAm_Status__c == 'Recruiter Review' || currentApp.TransAm_Status__c == 'Manager Review' || currentApp.TransAm_Status__c == 'Application Approved - Pending' 
               || currentApp.TransAm_Status__c == 'Schedule Orientation' || currentApp.TransAm_Status__c == 'Pending Orientation' || currentApp.TransAm_Status__c == 'HR Review' || currentApp.TransAm_Status__c == 'HR Approval' || currentApp.TransAm_Status__c == 'Merged'){
            for(RVI_XML_Settings__mdt objSetting : metasetting){
                for(formatXML objXML : xmlNodes){
                    if(String.isNotBlank(objSetting.TransAm_Task_X_In_Progress__c) && objXML.recruitDocType == objSetting.TransAm_Document_Name__c){
                        objXML.scanCode = objSetting.TransAm_RVI_Scan_Code__c;
                        objXML.folderLocation = objSetting.TransAm_Task_X_In_Progress__c;
                        Date hireUpdatedDate=Date.today();
                        objXML.hireDate = (datetime.newInstance(hireUpdatedDate.year(), hireUpdatedDate.month(),hireUpdatedDate.day())).format('MMddyy');
                        
                    }
                }
                
            }
        }
        else if(currentApp.TransAm_Status__c == 'DQ'){
            for(RVI_XML_Settings__mdt objSetting : metasetting){
                for(formatXML objXML : xmlNodes){
                    if(String.isNotBlank(objSetting.TransAm_Task_21_RejectedDQed__c) && objXML.recruitDocType == objSetting.TransAm_Document_Name__c){
                        objXML.scanCode = objSetting.TransAm_RVI_Scan_Code__c;
                        objXML.folderLocation = objSetting.TransAm_Task_21_RejectedDQed__c;
                        Date hireUpdatedDate=Date.today();
                        objXML.hireDate = (datetime.newInstance(hireUpdatedDate.year(), hireUpdatedDate.month(),hireUpdatedDate.day())).format('MMddyy');
                    }
                }
                
            }
        }
        else if(currentApp.TransAm_Status__c == 'No Contact'){
            for(RVI_XML_Settings__mdt objSetting : metasetting){
                for(formatXML objXML : xmlNodes){
                    if(String.isNotBlank(objSetting.TransAm_Task_25_No_Contact__c) && objXML.recruitDocType == objSetting.TransAm_Document_Name__c){
                        objXML.scanCode = objSetting.TransAm_RVI_Scan_Code__c;
                        objXML.folderLocation = objSetting.TransAm_Task_25_No_Contact__c;
                        Date hireUpdatedDate=Date.today();
                        objXML.hireDate = (datetime.newInstance(hireUpdatedDate.year(), hireUpdatedDate.month(),hireUpdatedDate.day())).format('MMddyy');
                    }
                }
                
            }
        }
        System.debug('##RA-->'+xmlNodes);
        String fileContent,fileContent2;
        for(formatXML objXML : xmlNodes){
            if(String.isBlank(objXML.driverType)){
                objXML.SC=objXML.folderLocation;
                if(String.isBlank(fileContent)){
                    fileContent=getxmlFormattedString(currentApp,objXML);
                    //fileContent = 'IndexField Name="SC" Value="'+objXML.folderLocation+'"\n';
                }
                else{
                    fileContent +=getxmlFormattedString(currentApp,objXML);
                    /*
                    fileContent += 'IndexField Name="SC" Value="'+objXML.folderLocation+'"\n';
                fileContent += 'IndexField Name="01" Value="'+currentApp.TransAm_DriverID__c+'"\n';
                fileContent += 'IndexField Name="02" Value="'+currentApp.TransAm_Last_Name__c+'"\n';
                fileContent += 'IndexField Name="03" Value="'+currentApp.TransAm_First_Name__c+'"\n';
                fileContent += 'IndexField Name="04" Value="'+currentApp.TransAm_SSN__c+'"\n';
                fileContent += 'IndexField Name="05" Value="'+objXML.hireDate+'"\n';
                fileContent += 'IndexField Name="06" Value="'+currentApp.Name+'"\n';
                fileContent += 'IndexField Name="07" Value="'+objXML.scanCode+'"\n';
                fileContent += 'IndexField Name="TY" Value="'+objXML.typeofDoc+'"\n';
                fileContent += 'IndexField Name="FL" Value="'+objXML.docName+'"\n';
                */
                }
            }
            else{
                objXML.SC=objXML.driverType;
                if(String.isBlank(fileContent2)){
                    fileContent2=getxmlFormattedString(currentApp,objXML);
                    //fileContent2 = 'IndexField Name="SC" Value="'+objXML.driverType+'"\n';
                }
                else{
                    fileContent2 +=getxmlFormattedString(currentApp,objXML);
                    /*
                    fileContent2 += 'IndexField Name="SC" Value="'+objXML.driverType+'"\n';
                
                	fileContent2 += 'IndexField Name="01" Value="'+currentApp.TransAm_DriverID__c+'"\n';
                    fileContent2 += 'IndexField Name="02" Value="'+currentApp.TransAm_Last_Name__c+'"\n';
                    fileContent2 += 'IndexField Name="03" Value="'+currentApp.TransAm_First_Name__c+'"\n';
                	fileContent2 += 'IndexField Name="04" Value="'+currentApp.TransAm_SSN__c+'"\n';
                	fileContent2 += 'IndexField Name="05" Value="'+objXML.hireDate+'"\n';
                	fileContent2 += 'IndexField Name="06" Value="'+currentApp.Name+'"\n';
                    fileContent2 += 'IndexField Name="07" Value="'+objXML.scanCode+'"\n';
                	fileContent2 += 'IndexField Name="TY" Value="'+objXML.typeofDoc+'"\n';
                    fileContent2 += 'IndexField Name="FL" Value="'+objXML.docName+'"\n'; 
                    */
                }
            }
        }
        System.debug('fileContent --> '+ fileContent);
        TransAm_Recruitment_Document__c objRecruit;
        if(String.isNotBlank(fileContent)){
            fileContent = fileContent.remove('null');
            objRecruit = [SELECT Id FROM TransAm_Recruitment_Document__c WHERE TransAm_Type__c = 'System Document' AND TransAm_Application__c =: appId LIMIT 1];
            //objRecruit = new TransAm_Recruitment_Document__c(TransAm_Application__c =appId, TransAm_Type__c ='System Document');
            //INSERT objRecruit;
            System.debug('objRecruit'+objRecruit);
            List<Attachment> existingSystemDoc = [SELECT ID FROM Attachment WHERE ParentId =: objRecruit.Id];
            if(existingSystemDoc.size()>0){
                DELETE existingSystemDoc;
            }
            UPDATE recruitmentDocList;
            Attachment att = new Attachment(Name = 'Index.xml', body = Blob.valueOf(fileContent), parentId = objRecruit.Id);
            updateAttachList.add(att);
        }
        if(String.isNotBlank(fileContent2)){
            fileContent2 = fileContent2.remove('null');
            Attachment att2 = new Attachment(Name = 'Index_2.xml', body = Blob.valueOf(fileContent2), parentId = objRecruit.Id);
            updateAttachList.add(att2);
        }
        if(!updateAttachList.isEmpty()){
            Database.INSERT(updateAttachList);
            currentApp.TransAm_RVI_Export_Date__c = Date.today();
            currentApp.TransAm_RVI_Export__c = true;
            UPDATE currentApp;
        }
    }
    public static String getxmlFormattedString(TransAm_Application__c currentApp,formatXML xmlObj){
        try{
            String resourceName='server_'+xmlObj.SC;
            String content=[SELECT Body FROM StaticResource WHERE Name =:resourceName].Body.toString();
            // update value in content from current application
            if(currentApp!=null){
               Map<String, object> currentAppMap=(Map<String, object>)JSON.deserializeUntyped(JSON.serialize(currentApp));
               for(String currentAppField: currentAppMap.keyset()){
                   content=content.replace('{currentApp.'+currentAppField+'}', (JSON.serialize(currentAppMap.get(currentAppField))).replace('"',''));
               }
            }
            // update value in content from format xml object
            if(xmlObj!=null){
               Map<String, object> xmlObjMap=(Map<String, object>)JSON.deserializeUntyped(JSON.serialize(xmlObj));
               for(String xmlObjField: xmlObjMap.keyset()){
                   content=content.replace('{objXML.'+xmlObjField+'}', (JSON.serialize(xmlObjMap.get(xmlObjField))).replace('"',''));
               }
            }
            content=(content).replaceAll('\\{[0-9A-Za-z\\_.]*\\}','');
            return content;
        }catch(Exception e){
            return '';
        }
    }
    global class formatXML{
        public String recruitDocType{get;set;}
        public Id docId {get;set;}
        public String docName {get;set;}
        public String scanCode{get;set;}
        public String typeofDoc{get;set;}
        public String folderLocation{get;set;}
        public String driverType {get;set;}
        public String hireDate {get;set;}
        public String SC{get; set;}
        
    }

}