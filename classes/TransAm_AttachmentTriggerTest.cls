/* CLass Name   : TransAm_AttachmentTriggerTest
 * Description  : Test class for Attachment Trigger
 * Created By   : Karthik Gulla
 * Created On   : 12-Apr-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Karthik Gulla               12-Apr-2017              Initial version.
 *
 *****************************************************************************************/
@isTest
private with sharing class TransAm_AttachmentTriggerTest {
    public static List<TransAm_Job_History__c> lstApplicationJobHistory;
    public static List<TransAm_Application__c> lstNewApplications;
    public static List<Attachment> lstAttachments;

    /************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
        List<Account> lstAccounts = TransAm_Test_DataUtility.createAccounts(1, true);
        List<Contact> lstContacts = TransAm_Test_DataUtility.createContacts(1, lstAccounts[0].Id, 'Applicant', true);
        List<TransAm_Application__c> lstApplications = TransAm_Test_DataUtility.createApplications(1, 'Company Driver Only', 'Application Received', lstContacts[0],true);
        List<TransAm_Employer__c> lstEmployers = TransAm_Test_DataUtility.createEmployers(1, true);                              
        TransAm_Test_DataUtility.createJobHistory(1, lstApplications[0], lstEmployers[0], true);
        List<TransAm_Recruitment_Document__c> lstRecrDocuments = [SELECT Id, TransAm_Type__c FROM TransAm_Recruitment_Document__c 
                                                                    WHERE TransAm_Application__c =: lstApplications[0].Id 
                                                                    AND (TransAm_Type__c = 'Background Check Release Form' 
                                                                        OR TransAm_Type__c ='Previous Employment Verification'
                                                                        OR TransAm_Type__c = 'School Verification Release Form')];
        TransAm_Test_DataUtility.createAttachments(1, lstRecrDocuments[0].Id, true);
        TransAm_Test_DataUtility.createAttachments(1, lstRecrDocuments[1].Id, true);
        TransAm_Test_DataUtility.createAttachments(1, lstRecrDocuments[2].Id, true);
    } 

    /************************************************************************************
    * Method       :    testAttachmentTrigger
    * Description  :    Test attachment trigger Functionalities
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testMethod void testAttachmentTrigger() {
        lstApplicationJobHistory = [SELECT Id, TransAm_Application__c FROM TransAm_Job_History__c];
        TransAm_SendFax.sendFaxToEmployer(lstApplicationJobHistory[0].Id, '16157507314');

        lstNewApplications = [SELECT Id FROM TransAm_Application__c];
        TransAm_SendFax.sendFaxToSchoolContact(lstNewApplications[0].Id);

        try{
            List<TransAm_Recruitment_Document__c> lstRecrDocumentsTest = [SELECT Id, TransAm_Type__c FROM TransAm_Recruitment_Document__c 
                                                                    WHERE TransAm_Application__c =: lstApplicationJobHistory[0].TransAm_Application__c 
                                                                    AND (TransAm_Type__c = 'Background Check Release Form' 
                                                                        OR TransAm_Type__c ='Previous Employment Verification'
                                                                        OR TransAm_Type__c = 'School Verification Release Form')];
            lstAttachments = TransAm_Test_DataUtility.createAttachments(1, lstRecrDocumentsTest[0].Id, false);
            lstAttachments[0].Name = 'TestUnsupportedDoc.xml';
            insert lstAttachments;
        }
        catch(Exception e) {
            Boolean expectedExceptionThrown =  e.getMessage().contains(Label.TransAm_FileType) ? true : false;
            System.assertEquals(expectedExceptionThrown, true);
        }
    }
}