/* Class Name   : TAFS_DocuSignUtility 
 * Description  : TUtility class for sending documents to contacts associated with leads using docusign
 * Created By   : Pankaj Singh
 * Created On   : 25-Jun-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj SIngh                22-Jun-2016             Initial version.
 *
 *****************************************************************************************/
public with sharing class TAFS_DocuSignUtility {
    
    public string applicantId {get;set;}
    private string receipientList = '';
    private decimal routingNo = 0;
    
    public TAFS_DocuSignUtility(){
        applicantId = ApexPages.currentPage().getParameters().get('Id');
    }
    public PageReference sendToDocuSign(){
    
        TransAm_Application__c appliationObj;
        appliationObj = [Select id,Owner.Name,TransAm_First_Name__c,Owner.Email,TransAm_Last_Name__c,TransAm_Email__c from TransAm_Application__c WHERE ID =: applicantId];        
        string RC = '';
        string RSL='';
        string RSRO='';
        string RROS='';
        string CCRM='';
        string CCTM='';
        string CCNM='';
        string CRCL=''; 
        string CRL='';
        string OCO='';
        string DST='';
        string LA='';
        string CEM='';
        string CES='';
        string STB='';
        string SSB='';
        string SES='';
        string SEM='';string SRS='';string SCS ='';string RES=''; 
        //DST='ab8269a7-474a-458d-8797-118ca95d8bc1'; 
        DST=''; 
        //Adding Notes & Attachments 
        LA='0'; 
        //Custom Recipient List 
        receipientList = receipientList +'Email~'+appliationObj.TransAm_Email__c+';FirstName~'+appliationObj.TransAm_First_Name__c+';LastName~'+appliationObj.TransAm_Last_Name__c+';Role~Signer1;RoutingOrder~1';
        
        CRL= receipientList + 'LoadDefaultContacts~0'; 
        //Custom Email Subject 
        CES='Consumer Report & Investigative Release Form'; 
        //Custom Email Message 
        CEM= 'Envelope sent by '+ appliationObj.Owner.Name+'(' + appliationObj.Owner.Email +')'+ ' for ' +appliationObj.TransAm_First_Name__c; 
        // Show Email Subject (default in config) 
        SES = '1'; //Ex: '1' 
        // Show Email Message (default in config) 
        SEM = '1'; //Ex: '1' 
        // Show Tag Button (default in config) 
        STB = '1'; //Ex: '1' 
        // Show Chatter (default in config) 
        SCS = '0'; //Ex: '1' 
        OCO = ''; 
        PageReference pageRef = null;
        pageRef = new PageReference('/apex/dsfs__DocuSign_CreateEnvelope?DSEID=0&SourceID='+applicantId+'&RC='+RC+'&RSL='+RSL+'&RSRO='+RSRO+'&RROS='+RROS+'&CCRM='+CCRM+'&CCTM='+CCTM+'&CRCL='+CRCL+'&CRL='+CRL+'&OCO='+OCO+'&DST='+DST+'&CCNM='+CCNM+'&LA='+LA+'&CEM='+CEM+'&CES='+CES+'&SRS='+SRS+'&STB='+STB+'&SSB='+SSB+'&SES='+SES+'&SEM='+SEM+'&SRS='+SRS+'&SCS='+SCS+'&RES='+RES);
        pageRef.setRedirect(true);    
        return pageRef;
   }
}