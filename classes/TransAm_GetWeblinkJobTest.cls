/* CLass Name   : TransAm_HireRightServiceConsumerTest
 * Description  : Test class for TransAm_HireRightServiceConsumer
 * Created By   : Monalisa Das
 * Created On   : 09-May-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Monalisa Das                09-May-2017              Initial version.
 *  
 *
 *****************************************************************************************/
 @isTest(seeAllData=false)
 public class TransAm_GetWeblinkJobTest {
 
/************************************************************************************
* Method       :    setup
* Description  :    setup test data
* Parameter    :    NIL    
* Return Type  :    void
*************************************************************************************/
@testSetup static void setup() {
        List<Account> lstAccounts = TransAm_Test_DataUtility.createAccounts(1, true);
        List<Contact> lstContacts = TransAm_Test_DataUtility.createContacts(1, lstAccounts[0].Id, 'Applicant', true);
        List<TransAm_Application__c> lstApplications = TransAm_Test_DataUtility.createApplications(1, 'Company Driver Only', 'Prospect', lstContacts[0], True);        
        System.debug('lstApplications ::'+lstApplications);
        Profile p = [SELECT id,name FROM Profile where name = 'TransAm_Integration_User'];
        system.assert(p.Id != null);
        User user_int = new User();
        user_int.FirstName = 'Test';
        user_int.LastName = 'User';
        user_int.Username = 'testHRuser@abc.com';
        user_int.Email   = 'testuser@deloitte.com';
        user_int.Alias   = 'xyz';
        user_int.TimeZoneSidKey   = 'America/Los_Angeles';
        user_int.LocaleSidKey   = 'en_US';
        user_int.EmailEncodingKey   = 'UTF-8';
        user_int.ProfileId = p.Id;                               
        user_int.LanguageLocaleKey = 'en_US';    
        System.debug('Logged In Userid:'+ UserInfo.getUserId());
        insert user_int;
        
	// List<TransAm_Application__c> lstApplications = [SELECT ID , TransAm_EBE_Entry_ID__c FROM TransAm_Application__c LIMIT 1];
	TransAm_HireRightHandler.prohibitAfterInsertTrigger = true;
        List<TransAm_HireRight_Orders__c> lstHROrders = TransAm_Test_DataUtility.createHireRightOrders(1, lstApplications[0].Id, 'MVR Standard' ,True); 
    system.debug(LoggingLevel.ERROR, 'app id ' + lstApplications[0].Id);
    system.debug(LoggingLevel.ERROR,'app name ' + lstApplications[0].Name);
	TransAm_HireRightHandler.prohibitAfterInsertTrigger = false;
	TransAm_HireRightHandler.prohibitAfterUpdateTrigger = true;
	TransAm_HireRight_Orders__c order = lstHROrders.get(0);
	order.TransAm_Hireright_Order_ID__c = 'TN-050917-JH8M3';
	update order;
	TransAm_HireRightHandler.prohibitAfterUpdateTrigger = false;
	
	
    }
    
    /************************************************************************************
* Method       : TransAm_GetWeblistTest
* Description  :    setup test data
* Parameter    :    NIL    
* Return Type  :    void
*************************************************************************************/
    public static testmethod void TransAm_GetWeblistTest() {
        
       User user_int = [SELECT ID FROM USER  WHERE Username =: 'testHRuser@abc.com' LIMIT 1];
        Test.startTest();
        System.runAs(user_int){ 
          Map<String, String> headers = new map<String, String>();
            headers.put('Location', 'sforce');
	     TransAm_HRServiceMock fakeResponse3 = new TransAm_HRServiceMock(200,
						'SUCCESS',
						'<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"><SOAP-ENV:Header/><SOAP-ENV:Body><hr_objs:GenerateWebLinkResponse xmlns:hr_objs="urn:enterprise.soap.hireright.com/objs" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><hr_objs:Result xsi:type="hr_objs:ReportWL"><hr_objs:RegId>TN-050917-JH8M3</hr_objs:RegId><hr_objs:ReportContent>FULL</hr_objs:ReportContent><hr_objs:ReportURL contentType="application/pdf">https://ows01.hireright.com/screening_manager/entry?key=E1C083547A0E0B376420D5EE71B2AC6A</hr_objs:ReportURL></hr_objs:Result></hr_objs:GenerateWebLinkResponse></SOAP-ENV:Body></SOAP-ENV:Envelope>',
                                                 headers);
	    Test.setMock(HttpCalloutMock.class, fakeResponse3);
	   
	    TransAm_HireRight_Orders__c order = [SELECT Name,TransAm_Application__c,TransAm_HireRight_Reports__c,TransAm_Report_Type__c,TransAm_Hireright_Order_ID__c, TransAm_Application__r.Name from TransAm_HireRight_Orders__c where TransAm_Hireright_Order_ID__c = 'TN-050917-JH8M3'];
	    //TransAm_HireRight_Orders__c order = [select id,Name, TransAm_Application__c, TransAm_Application__r.Name, TransAm_Report_Type__c,TransAm_Hireright_Order_ID__c from TransAm_HireRight_Orders__c where TransAm_Hireright_Order_ID__c = 'TN-050917-JH8M3'];
	    
	     String soapRequest = TransAm_HRRequestService.getGenerateWeblinkRequest('TN-050917-JH8M3');
	     TransAm_GetWeblinkJob testBatch= new TransAm_GetWeblinkJob(order.Name,soapRequest);
	     QueueableContext context;
	     testBatch.execute(context);
	     //System.enqueueJob(testBatch);

        }
	 Test.stopTest();
    }
    
    
    /************************************************************************************
* Method       : TransAm_GetWeblistExceptionTest
* Description  :    setup test data
* Parameter    :    NIL    
* Return Type  :    void
*************************************************************************************/
    public static testmethod void TransAm_GetWeblistExceptionTest() {
        
       try{
       User user_int = [SELECT ID FROM USER  WHERE Username =: 'testHRuser@abc.com' LIMIT 1];
        Test.startTest();
        System.runAs(user_int){ 
          Map<String, String> headers = new map<String, String>();
            headers.put('Location', 'sforce');
	     TransAm_HRServiceMock fakeResponse3 = new TransAm_HRServiceMock(200,
						'SUCCESS',
						'<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"><SOAP-ENV:Header/><SOAP-ENV:Body><hr_objs:GenerateWebLinkResponse xmlns:hr_objs="urn:enterprise.soap.hireright.com/objs" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><hr_objs:Result xsi:type="hr_objs:ReportWL"><hr_objs:RegId>TN-050917-JH8M3</hr_objs:RegId><hr_objs:ReportContent>FULL</hr_objs:ReportContent><hr_objs:ReportURL contentType="application/pdf">https://ows01.hireright.com/screening_manager/entry?key=E1C083547A0E0B376420D5EE71B2AC6A</hr_objs:ReportURL></hr_objs:Result></hr_objs:GenerateWebLinkResponse></SOAP-ENV:Body></SOAP-ENV:Envelope>',
                                                 headers);
	    Test.setMock(HttpCalloutMock.class, fakeResponse3);
	   
	    TransAm_HireRight_Orders__c order = [SELECT Name,TransAm_Application__c,TransAm_HireRight_Reports__c,TransAm_Report_Type__c,TransAm_Hireright_Order_ID__c, TransAm_Application__r.Name from TransAm_HireRight_Orders__c where TransAm_Hireright_Order_ID__c = 'TN-050917-JH8M3'];
	    //TransAm_HireRight_Orders__c order = [select id,Name, TransAm_Application__c, TransAm_Application__r.Name, TransAm_Report_Type__c,TransAm_Hireright_Order_ID__c from TransAm_HireRight_Orders__c where TransAm_Hireright_Order_ID__c = 'TN-050917-JH8M3'];
	    
	     String soapRequest = TransAm_HRRequestService.getGenerateWeblinkRequest('TN-050917-JH8M3');
	     TransAm_GetWeblinkJob testBatch= new TransAm_GetWeblinkJob('abc',soapRequest);
	     QueueableContext context;
	     testBatch.execute(context);
	     //System.enqueueJob(testBatch);

        }
	 Test.stopTest();
	 }
	 catch(Exception e)
	 {
	
	 }
    }
}