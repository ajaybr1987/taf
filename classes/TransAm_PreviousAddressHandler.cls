/* Class Name   : TransAm_PreviousAddressHandler
 * Description  : Handler class for the Previosu Address trigger
 * Created By   : Pankaj Singh
 * Created On   : 14-Feb-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh               14-Feb-2017              Initial version.
 *
 *****************************************************************************************/
public class TransAm_PreviousAddressHandler{

    public static boolean prohibitBeforeInsertTrigger = false;
    
    /****************************************************************************************
    * Created By      :  Pankaj Singh
    * Create Date     :  13-Feb-2017
    * Description     :  handler method for after insert event
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public static void beforeInsertHandler(list<TransAm_PreviousAddress__c> addressList){
    
        if(prohibitBeforeInsertTrigger){
            return;
        }        
        TransAm_PreviousAddressHelper.populateApplicationOnAdress(addressList);
        prohibitBeforeInsertTrigger = true;
    }

}