/* Trigger Name : TransAm_UpdateApplicationStatus
 * Description  : Trigger for DocuSignStatus object
 * Created By   : Pankaj Singh
 * Created On   : 19-Mar-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh                19-Mar-2017             Initial version.
 *
 *****************************************************************************************/
trigger TransAm_DocuSignStatusTrigger on dsfs__DocuSign_Status__c (after insert, after update) {
    if(Trigger.isInsert){
        if(Trigger.isAfter){
            TransAm_DocusignStatusHandler.afterInsertHandler(trigger.new);
        }
    }
    if(Trigger.isUpdate){
        if(Trigger.isAfter){
            TransAm_DocusignStatusHandler.afterUpdateHandler(trigger.new);
        }
    }
}