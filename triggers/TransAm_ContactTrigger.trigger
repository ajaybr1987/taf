/******************************************************************************************
* Create By    :     Suresh M
* Create Date  :     02/14/2017
* Description  :     A common trigger for Contact object for all the events.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * <Name>                             <Date>           Initial version.
*****************************************************************************************/

trigger TransAm_ContactTrigger on Contact (before insert, before update,after insert, after undelete) {
    
    if(trigger.isBefore && trigger.isInsert){        
        TransAm_ContactHandler.beforeInsertHandler(trigger.new);            
    }
    
    if(trigger.isBefore && trigger.isUpdate){
        TransAm_ContactHandler.beforeUpdateHandler(trigger.new, trigger.oldMap);
    }
    
    if(trigger.isAfter && trigger.isUndelete){
        TransAm_ContactHandler.afterUndeleteHandler(trigger.new);
    }  
    
    if(trigger.isAfter && trigger.isInsert){
        TransAm_ContactHandler.afterInsertHandler(trigger.new, trigger.newMap, trigger.oldMap);
    }  
}