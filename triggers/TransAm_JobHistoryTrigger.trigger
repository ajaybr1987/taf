/* Trigger Name : TransAm_JobHistoryTrigger
 * Description  : Trigger for Violation object
 * Created By   : Pankaj Singh
 * Created On   : 17-Feb-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh               17-Feb-2017              Initial version.
 *
 *****************************************************************************************/
trigger TransAm_JobHistoryTrigger on TransAm_Job_History__c(before insert) {
    if(Trigger.isInsert){
        if(Trigger.isBefore){
            TransAm_JobHistoryHandler.beforeInsertHandler(trigger.new);
        }
    }

}