trigger TransAm_TaskTrigger on Task (after insert) {
    if(Trigger.isinsert){
        if(Trigger.isAfter){
            TransAm_TaskHandler.afterInsertHandler(trigger.new);
        }
    }
}