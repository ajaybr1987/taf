/******************************************************************************************
* Create By    :     Pardeep Rao
* Create Date  :     03/06/2017
* Description  :     A common trigger for Account object for all the events.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * <Name>                             <Date>           Initial version.
*****************************************************************************************/

trigger TransAm_AccountTrigger on Account (after insert) {
    
    if(trigger.isafter && trigger.isInsert){        
        TransAm_AccountHandler.afterInsertHandler(trigger.new);            
    }
   
}