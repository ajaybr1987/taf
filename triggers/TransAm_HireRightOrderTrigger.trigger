/* Trigger Name : TransAm_HireRightOrderTrigger
 * Description  : Trigger for HireRightOrder object
 * Created By   : Monalisa
 * Created On   : 9-Apr-2017
 *
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Monalisa               9-Apr-2017              Initial version.
 *
 *****************************************************************************************/
trigger TransAm_HireRightOrderTrigger on TransAm_HireRight_Orders__c (after insert, after update) {

    if(Trigger.isInsert)
    {
        if(Trigger.isAfter){
            TransAm_HireRightHandler.afterInsertHandler(trigger.new);
        }
     }
    
	if(Trigger.isUpdate)
    {
        if(Trigger.isAfter){
            TransAm_HireRightHandler.afterUpdatetHandler(trigger.new);
        }
     }    

}