/* Trigger Name : TransAm_AttachmentTrigger
 * Description  : Trigger for Attachment object
 * Created By   : Pankaj Singh
 * Created On   : 19-Mar-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh                19-Mar-2017             Initial version.
 *
 *****************************************************************************************/
trigger TransAm_AttachmentTrigger on Attachment (after insert, after Update,before insert) {
    if(Trigger.IsInsert){
        if(Trigger.isAfter){
            TransAm_AttachmentHandler.afterInsertHandler(trigger.new);                 
        }
        if(Trigger.isBefore){
            TransAm_AttachmentHandler.beforeInsertHandler(trigger.new);
        }
        
    }
    if(Trigger.IsUpdate){
        if(Trigger.isAfter){
            TransAm_AttachmentHandler.afterUpdateHandler(trigger.new);                 
        }
    }
}