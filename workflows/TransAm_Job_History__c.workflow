<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>TransAm_Populate_Other_Employer</fullName>
        <field>TransAm_Other_Employer__c</field>
        <formula>TransAm_Employer2__r.Name</formula>
        <name>TransAm_Populate_Other_Employer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>TransAm_Copy_Employer_To_Other_Employer</fullName>
        <actions>
            <name>TransAm_Populate_Other_Employer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>TransAm_Job_History__c.TransAm_Other_Employer__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Copy employer name from employer lookup to other employer field.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
