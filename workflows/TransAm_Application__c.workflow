<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Air_Travel_Confirmation_Olathe</fullName>
        <description>Air Travel Confirmation ,Olathe</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Olathe_Air_Welcome_Mail</template>
    </alerts>
    <alerts>
        <fullName>Air_Travel_Confirmation_Rockwall</fullName>
        <description>Air Travel Confirmation ,Rockwall</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Rockwall_Air_Welcome_Mail</template>
    </alerts>
    <alerts>
        <fullName>Bus_Confirmation_For_Rockwall</fullName>
        <description>Bus Confirmation For Rockwall</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Rockwall_Bus_Welcome_Mail</template>
    </alerts>
    <alerts>
        <fullName>Bus_Travel_Confirmation_To_Travelers_For_Olathe</fullName>
        <description>Bus Travel Confirmation To Travelers For Olathe</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Olathe_Bus_Welcome_Mail</template>
    </alerts>
    <alerts>
        <fullName>No_Car_Hotel_Olathe</fullName>
        <description>Personal Vehicle/ Rental Car Travel And No Lodging Confirmation To Travelers For Olathe</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Olathe_Car_No_Hotel</template>
    </alerts>
    <alerts>
        <fullName>Personal_Vehicle_Rental_Car_Travel_And_Lodging_Confirmation_Olathe</fullName>
        <description>Personal Vehicle/ Rental Car Travel And Lodging Confirmation To Travelers For Olathe</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Olathe_Car_Hotel</template>
    </alerts>
    <alerts>
        <fullName>Personal_Vehicle_Rental_Car_Travel_And_Lodging_Confirmation_To_Travelers_For_Ola</fullName>
        <description>Personal Vehicle/ Rental Car Travel And Lodging Confirmation To Travelers For Olathe</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Olathe_Car_No_Hotel</template>
    </alerts>
    <alerts>
        <fullName>Personal_Vehicle_Rental_Car_Travel_And_Lodging_Confirmation_To_Travelers_For_Roc</fullName>
        <description>Personal Vehicle/ Rental Car Travel And Lodging Confirmation To Travelers For Rockwall</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Rockwall_Car_Welcome_Mail</template>
    </alerts>
    <alerts>
        <fullName>Personal_Vehicle_Rental_Car_Travel_Confirmation_To_Travelers_For_Olathe</fullName>
        <description>Personal Vehicle/ Rental Car Travel Confirmation To Travelers For Olathe</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Olathe_Car_No_Hotel</template>
    </alerts>
    <alerts>
        <fullName>Personal_Vehicle_Rental_Car_Welcome_Mail</fullName>
        <description>Personal Vehicle/ Rental Car Welcome Mail Without Lodging Details</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Rockwall_Car_No_Hotel_Welcome_Mail</template>
    </alerts>
    <alerts>
        <fullName>TransAm_Email_To_Drivers_travelling_by_Fight_to_Texas</fullName>
        <description>TransAm Email To Drivers travelling by Fight to Texas</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/TransAm_Air_Travel_Confirmation_Rockwall</template>
    </alerts>
    <alerts>
        <fullName>TransAm_Email_To_Drivers_travelling_to_Olathe</fullName>
        <description>TransAm  Welcome Email To Drivers Travelling To Olathe</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/TransAm_Travel_Confirmation_For_Olathe</template>
    </alerts>
    <alerts>
        <fullName>TransAm_Email_To_Drivers_travelling_to_Rockwall</fullName>
        <description>TransAm Travel Confirmation For Applicants Travelling To Rockwall</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/TransAm_Travel_Confirmation_For_Rockwall</template>
    </alerts>
    <alerts>
        <fullName>TransAm_Travel_Confirmation_to_Olathe</fullName>
        <description>TransAm Travel Confirmation to Olathe</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/TransAm_Travel_Confirmation_For_Olathe</template>
    </alerts>
    <alerts>
        <fullName>TransAm_Welcome_Email_To_Drivers_Travelling_To_Rockwall</fullName>
        <description>TransAm  Welcome Email To Drivers Travelling To Rockwall</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/TransAm_Travel_Confirmation_For_Rockwall</template>
    </alerts>
    <alerts>
        <fullName>Welcome_Email_For_Drivers_Travelling_by_Bus_To_Rockwall</fullName>
        <description>Welcome Email For Drivers Travelling by Bus To Rockwall</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/TransAm_Bus_Travel_Confirmation_For_Rockwall</template>
    </alerts>
    <alerts>
        <fullName>Welcome_Email_For_Drivers_Travelling_by_Car_Without_Lodging_To_Rockwall</fullName>
        <description>Welcome Email For Drivers Travelling by Car Without Lodging To Rockwall</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Olathe_Car_No_Hotel</template>
    </alerts>
    <alerts>
        <fullName>car_Hotel_KS</fullName>
        <description>car Hotel KS</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Olathe_Car_Hotel</template>
    </alerts>
    <fieldUpdates>
        <fullName>TransAm_Application_Uncheck</fullName>
        <field>TransAm_Is_status_Changed_Through_Merge__c</field>
        <literalValue>0</literalValue>
        <name>Application_Uncheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TransAm_ICC_Export</fullName>
        <field>TransAm_ICC_Export__c</field>
        <literalValue>1</literalValue>
        <name>ICC Export</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TransAm_ICC_Export_Date</fullName>
        <field>TransAm_ICC_Export_Date__c</field>
        <formula>TODAY()</formula>
        <name>ICC Export Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TransAm_StatusFlag_Update</fullName>
        <field>TransAm_Status_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Status Flag Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TransAm_Status_Flag_Update</fullName>
        <field>TransAm_Status_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Status Flag Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TransAm_Update_Archive_Date</fullName>
        <field>TransAm_Archived_Date__c</field>
        <formula>Today()</formula>
        <name>Update Archive Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_By_Pass_Validation_Checkbox</fullName>
        <field>TransAm_By_Pass_Validation__c</field>
        <literalValue>1</literalValue>
        <name>Update By Pass Validation Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Received_Date_for_Manual_Apps</fullName>
        <field>TransAm_Received_Date__c</field>
        <formula>Today()</formula>
        <name>Update Received Date for Manual Apps</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Air Travel Confirmation To Olathe</fullName>
        <actions>
            <name>Air_Travel_Confirmation_Olathe</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Air Travel Confirmation To Olathe</description>
        <formula>ISPICKVAL(PRIORVALUE(TransAm_Status__c),&apos;Schedule Orientation&apos;)  &amp;&amp; ISPICKVAL(TransAm_Status__c ,&apos;Pending Orientation&apos;)  &amp;&amp;   ISPICKVAL(TransAm_Orientation_Location__c,&apos;Olathe, KS&apos;)   &amp;&amp; ISPICKVAL(TransAm_Transportation__c ,&apos;Flight&apos;) &amp;&amp;  (TransAm_Transportation_confirmation__c = True) &amp;&amp;   NOT(ISPICKVAL(TransAm_Lodging__c,&apos;&apos;)) &amp;&amp;  NOT( ISPICKVAL(Transam_Lodging_Address__c,&apos;&apos; ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Air Travel Confirmation To Travelers For Rockwall</fullName>
        <actions>
            <name>Air_Travel_Confirmation_Rockwall</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Air Travel Confirmation To Traveler Going To Rockwall</description>
        <formula>ISPICKVAL(PRIORVALUE(TransAm_Status__c),&apos;Schedule Orientation&apos;)  &amp;&amp; ISPICKVAL(TransAm_Status__c ,&apos;Pending Orientation&apos;)  &amp;&amp;   ISPICKVAL(TransAm_Orientation_Location__c,&apos;Rockwall, TX&apos;)   &amp;&amp; ISPICKVAL(TransAm_Transportation__c ,&apos;Flight&apos;) &amp;&amp;  (TransAm_Transportation_confirmation__c = True) &amp;&amp;   NOT(ISPICKVAL(TransAm_Lodging__c,&apos;&apos;)) &amp;&amp;  NOT( ISPICKVAL(Transam_Lodging_Address__c,&apos;&apos; ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Bus Travel Confirmation To Travelers For Olathe</fullName>
        <actions>
            <name>Bus_Travel_Confirmation_To_Travelers_For_Olathe</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Bus Travel Confirmation To Travelers For Olathe</description>
        <formula>ISPICKVAL(PRIORVALUE(TransAm_Status__c),&apos;Schedule Orientation&apos;)  &amp;&amp; ISPICKVAL(TransAm_Status__c ,&apos;Pending Orientation&apos;)  &amp;&amp;   ISPICKVAL(TransAm_Orientation_Location__c,&apos;Olathe, KS&apos;)   &amp;&amp; ISPICKVAL(TransAm_Transportation__c ,&apos;Bus&apos;) &amp;&amp;  (TransAm_Transportation_confirmation__c = True) &amp;&amp;   NOT(ISPICKVAL(TransAm_Lodging__c,&apos;&apos;)) &amp;&amp;  NOT( ISPICKVAL(Transam_Lodging_Address__c,&apos;&apos; ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Bus Travel Confirmation To Travelers For Rockwall</fullName>
        <actions>
            <name>Bus_Confirmation_For_Rockwall</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Bus Travel Confirmation To Travelers For Rockwall</description>
        <formula>ISPICKVAL(PRIORVALUE(TransAm_Status__c),&apos;Schedule Orientation&apos;)  &amp;&amp; ISPICKVAL(TransAm_Status__c ,&apos;Pending Orientation&apos;)  &amp;&amp;   ISPICKVAL(TransAm_Orientation_Location__c,&apos;Rockwall, TX&apos;)   &amp;&amp; ISPICKVAL(TransAm_Transportation__c ,&apos;Bus&apos;) &amp;&amp;  (TransAm_Transportation_confirmation__c = True) &amp;&amp;   NOT(ISPICKVAL(TransAm_Lodging__c,&apos;&apos;)) &amp;&amp;  NOT( ISPICKVAL(Transam_Lodging_Address__c,&apos;&apos; ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Completed Applicaton Rule</fullName>
        <active>true</active>
        <criteriaItems>
            <field>TransAm_Application__c.TransAm_Status__c</field>
            <operation>equals</operation>
            <value>Application Complete</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>TransAm_Update_Archive_Date</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Disqualified Application</fullName>
        <actions>
            <name>TransAm_Update_Archive_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>TransAm_Application__c.TransAm_Status__c</field>
            <operation>equals</operation>
            <value>DQ</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>In Progress Application Rule</fullName>
        <active>true</active>
        <criteriaItems>
            <field>TransAm_Application__c.TransAm_Status__c</field>
            <operation>equals</operation>
            <value>Application Received,Recruiter Review,Manager Review,Schedule Orientation,Pending Orientation,HR Review,HR Approval,Application Approved - Pending</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>TransAm_Update_Archive_Date</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>50</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Personal Vehicle%2F Rental Car Travel And Lodging Confirmation To Travelers For Rockwall</fullName>
        <actions>
            <name>Personal_Vehicle_Rental_Car_Travel_And_Lodging_Confirmation_To_Travelers_For_Roc</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Personal Vehicle/ Rental Car Travel And Lodging Confirmation To Travelers For Rockwall</description>
        <formula>ISPICKVAL(PRIORVALUE(TransAm_Status__c),&apos;Schedule Orientation&apos;)  &amp;&amp; ISPICKVAL(TransAm_Status__c ,&apos;Pending Orientation&apos;)  &amp;&amp;   ISPICKVAL(TransAm_Orientation_Location__c,&apos;Rockwall, TX&apos;)   &amp;&amp; OR(ISPICKVAL(TransAm_Transportation__c ,&apos;Personal Vehicle&apos;),ISPICKVAL(TransAm_Transportation__c ,&apos;Rental Car&apos;))&amp;&amp;  (TransAm_Transportation_confirmation__c = True) &amp;&amp;   NOT(ISPICKVAL(TransAm_Lodging__c,&apos;&apos;)) &amp;&amp;  NOT( ISPICKVAL(Transam_Lodging_Address__c,&apos;&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Personal Vehicle%2F Rental Car Travel Confirmation To Travelers For Olathe</fullName>
        <actions>
            <name>Personal_Vehicle_Rental_Car_Travel_Confirmation_To_Travelers_For_Olathe</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Personal Vehicle/ Rental Car Travel Confirmation To Travelers For Olathe</description>
        <formula>AND(ISPICKVAL(PRIORVALUE(TransAm_Status__c),&apos;Schedule Orientation&apos;),ISPICKVAL(TransAm_Status__c ,&apos;Pending Orientation&apos;),ISPICKVAL(TransAm_Orientation_Location__c ,&apos;Olathe, KS&apos;),OR(ISPICKVAL(TransAm_Transportation__c ,&apos;Personal Vehicle&apos;),ISPICKVAL(TransAm_Transportation__c ,&apos;Rental Car&apos;)),(TransAm_Transportation_confirmation__c = True),(ISPICKVAL(TransAm_Lodging__c,&apos;&apos;)) ,( ISPICKVAL(Transam_Lodging_Address__c,&apos;&apos; )))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Personal Vehicle%2F Rental Car Travel Confirmation To Travelers For Rockwall</fullName>
        <actions>
            <name>Personal_Vehicle_Rental_Car_Welcome_Mail</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Personal Vehicle/ Rental Car Travel Confirmation To Travelers For Rockwall</description>
        <formula>ISPICKVAL(PRIORVALUE(TransAm_Status__c),&apos;Schedule Orientation&apos;)  &amp;&amp; ISPICKVAL(TransAm_Status__c ,&apos;Pending Orientation&apos;)  &amp;&amp;   ISPICKVAL(TransAm_Orientation_Location__c,&apos;Rockwall, TX&apos;)   &amp;&amp; OR(ISPICKVAL(TransAm_Transportation__c ,&apos;Personal Vehicle&apos;),ISPICKVAL(TransAm_Transportation__c ,&apos;Rental Car&apos;))&amp;&amp;  (TransAm_Transportation_confirmation__c = True) &amp;&amp;   (ISPICKVAL(TransAm_Lodging__c,&apos;&apos;)) &amp;&amp;  ( ISPICKVAL(Transam_Lodging_Address__c,&apos;&apos; ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Status Flag Update</fullName>
        <actions>
            <name>TransAm_ICC_Export</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TransAm_ICC_Export_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TransAm_StatusFlag_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISPICKVAL(PRIORVALUE(TransAm_Status__c),&quot;Pending Orientation&quot;),ISPICKVAL(TransAm_Status__c,&quot;HR Review&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TransAm Travel Confirmation For Applicants Travelling To Olathe</fullName>
        <actions>
            <name>car_Hotel_KS</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(ISPICKVAL(PRIORVALUE(TransAm_Status__c),&apos;Schedule Orientation&apos;),ISPICKVAL(TransAm_Status__c ,&apos;Pending Orientation&apos;),ISPICKVAL(TransAm_Orientation_Location__c ,&apos;Olathe, KS&apos;),OR(ISPICKVAL(TransAm_Transportation__c ,&apos;Personal Vehicle&apos;),ISPICKVAL(TransAm_Transportation__c ,&apos;Rental Car&apos;)),(TransAm_Transportation_confirmation__c = True),NOT(ISPICKVAL(TransAm_Lodging__c,&apos;&apos;)) ,NOT( ISPICKVAL(Transam_Lodging_Address__c,&apos;&apos; )))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TransAm_Assign_Task_To_Recruiter</fullName>
        <actions>
            <name>TransAm_Follow_up_with_the_applicant</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT(ISNEW()), ISPICKVAL(PRIORVALUE(TransAm_Status__c), &apos;Manager Review&apos;), ISPICKVAL(TransAm_Status__c , &apos;Recruiter Review&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TransAm_Uncheck_Is_Status_Changed_Through_Merge</fullName>
        <actions>
            <name>TransAm_Application_Uncheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TransAm_Is_status_Changed_Through_Merge__c &amp;&amp; !ISBLANK( TransAm_Master_Application__c) &amp;&amp; !ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update By Pass Validation Checkbox</fullName>
        <actions>
            <name>Update_By_Pass_Validation_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>TransAm_Application__c.TransAm_By_Pass_Validation__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Received Date on manually Created Applications</fullName>
        <actions>
            <name>Update_Received_Date_for_Manual_Apps</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow will populate Received Date with current date when application is not created through integration</description>
        <formula>CreatedBy.Profile.Name &lt;&gt; &apos;TransAm_Integration_User&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>TransAm_Follow_up_with_the_applicant</fullName>
        <assignedToType>owner</assignedToType>
        <description>Follow up with the applicant.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>Follow up with the applicant</subject>
    </tasks>
</Workflow>
